var $ = jQuery.noConflict();

$(window).load(function(){
    //$('.yes-js.js_active.js body').css('opacity', '1');

    $('.add_to_cart_button').attr('title', 'Dodaj u korpu');

    var timer;

    $('.add_to_cart_button').click(function() {
        clearTimeout(timer);
        var message = document.createElement('span');
        message.innerText = 'Dodato u korpu';
        message.setAttribute('class','cart-message');
        $(this).closest('.owl-carousel-single-image, .vec-ste-gledali-single-item, .search-results-item, .nav-sub-menu-list-book-relative').append(message);
        setTimeout(function() {
            $(message).fadeOut();
        }, 2000 );

        timer = setTimeout(function() {
            $('.cart-reminder').css({
                'visibility': 'visible',
                'opacity': 1
            });
        }, 35000);

    });

});

$(function() {
	$('.owl-carousel-single-image, .vec-ste-gledali-wrapper, .search-results-item, .nav-sub-menu-list-book-relative').matchHeight();
    var owl = $('.owl-carousel');
    owl.on('changed.owl.carousel', function(event) {
        $('.owl-carousel-single-image').matchHeight();
    });
});

$(function() {
    var list = $('.woocommerce-order-received .chipcard-transaction-info');
    var new1 = $('.woocommerce-order-received h4.chipcard-transaction-title');
    var tmp = document.createElement('div');
    tmp.setAttribute('class','chipcard-transaction-parent');
    list.appendTo(tmp);
    $(new1).insertBefore($('.view-order-details'));
    $('.view-order-details').prepend(tmp);
});

// $(function() {
//     var tmp = $('.single-product-cat-mobile h4');
//     console.log(tmp);
//     tmp.click(function() {
//         if ($('.single-product-cat-mobile ul').hasClass('undisplayed')) {
//             $('.single-product-cat-mobile ul').removeClass('undisplayed').addClass('displayed');
//         }
//         else {
//             $('.single-product-cat-mobile ul').removeClass('displayed').addClass('undisplayed');
//         }
//     });
// });

$(window).load(function() {
    if ($('input.dgwt-wcas-search-input').val().length !== 0) {
        $('.widget_search button .dgwt-wcas-ico-loupe').css('fill', 'none');
    } else {
        $('.widget_search button .dgwt-wcas-ico-loupe').css('fill', '#66ca23');
    }
});

$(document).ready(function() {

    if ($(window).width() < 768) {
        $('.owl-carousel-single-image').click(function() {
            $('.owl-carousel-single-image').find('.hidden-article').addClass('unseen');
            $(this).find('.hidden-article').toggleClass('unseen');
        });
    } else {
        $('.owl-carousel-single-image').hover(function() {
            $(this).find('.hidden-article').toggleClass('unseen');
        });
    }

    var cartReminder = document.createElement("div");
    cartReminder.setAttribute('class','cart-reminder');

    var cartReminderHeading = document.createElement("h3");
    cartReminderHeading.innerHTML = 'Idi do korpe';

    var cartReminderText = document.createElement("p");
    cartReminderText.innerHTML = 'Idi do korpe';

    var cartReminderButton = document.createElement('a');
    cartReminderButton.innerHTML = 'Idi do korpe';
    cartReminderButton.setAttribute('class','go-tooo-cart');
    cartReminderButton.setAttribute('href', 'https://dev1.m1.rs/knd/placanje/');

    var careReminderCloseLink = document.createElement('a');
    careReminderCloseLink.setAttribute('class','close-reminder');

    cartReminder.appendChild(cartReminderHeading);
    cartReminder.appendChild(cartReminderText);
    cartReminder.appendChild(cartReminderButton);
    cartReminder.appendChild(careReminderCloseLink);
    document.body.appendChild(cartReminder);


    $('.close-reminder').click(function() {
        $('.cart-reminder').css({
            'visibility': 'hidden',
            'opacity': 0
        })
    });

    $('.hamburger').click (function(e){
        e.preventDefault();
    });

    $('form.giftwrapper_products.wcgwp_slideout.non_modal.wcgwp_form button#giftwrap_submit_before_cart').text('Pošaljite kao poklon');

    $('div.giftwrap_header_wrapper.gift-wrapper-info a').text('Pošaljite kao poklon');

    $('input.dgwt-wcas-search-input').on('input', function() {
        if ($(this).val().length !== 0) {
            $('.widget_search button .dgwt-wcas-ico-loupe').css('fill', 'none');
        } else {
            $('.widget_search button .dgwt-wcas-ico-loupe').css('fill', '#66ca23');
        }
    });

    $('input.dgwt-wcas-search-input').on('change', function() {
        if ($(this).val().length !== 0) {
            $('.widget_search button .dgwt-wcas-ico-loupe').css('fill', 'none');
        } else {
            $('.widget_search button .dgwt-wcas-ico-loupe').css('fill', '#66ca23');
        }
    });

    var tmp = $('.single-product-cat-mobile h4');
    tmp.click(function() {
        $('.single-product-cat-mobile ul').toggleClass("undisplayed");

        // if ($('.single-product-cat-mobile ul').hasClass('undisplayed')) {
        //     $('.single-product-cat-mobile ul').removeClass('undisplayed').addClass('displayed');
        // }
        // else {
        //     $('.single-product-cat-mobile ul').removeClass('displayed').addClass('undisplayed');
        // }
    });

    var search = $('.mobile-search');
    search.click(function() {
        $('.search-wrapper').toggleClass('undisplayed');
    });

    $(function() {
        if ($('.vec-ste-gledali-container').children().length == 0) {
            $('.vec-ste-gledali').css('display', 'none');
        }
    });

    $("tr.shipping th").text("Dostava");
    $('#checkout_giftwrap_submit').text('Pošaljite kao poklon');


    $(function() {
        var msg = document.querySelectorAll('.woocommerce-message');
        var el = document.createElement("div");
        el.innerHTML = msg;
        setTimeout(function(){
            el.parentNode.removeChild(el);
            }, 3000);
        document.body.appendChild(el);

    });



    $('.kategorije-heading').click(function() {
        $('.kategorije-menu ul, .single-product-kategorije-menu ul').toggle();
    });

    $('#menu-icon').click(function() {
        $('.mobile-nav').toggle();
        var display = $('.mobile-nav').css('display');
        if (display == 'block') {
          $('.site-header .main-navigation .widget_search').css('margin', '20px auto');
        }
        else {
          $('.site-header .main-navigation .widget_search').css('margin-left', '0');
        }
    });

    $('.clickable').click(function(e) {
        e.preventDefault();
        var el = $(this).parent().children(".primary-item-submenu");

        if (el.hasClass("undisplayed")) {
            el.addClass("displayed").removeClass("undisplayed");
            el.css({
                'overflow-y': 'scroll',
                'z-index': '999999',
                'margin-top': '65px',
                'padding-top': '35px'
            });
            $('.close-mobile-nav').removeClass('undisplayed').addClass('displayed');
            $('.site-logo').css('display', 'none');
            $('.hamburger').css('display', 'none');
            $('.mobile-nav > ul > li.primary-item > a').css('display', 'none');
            $('.site-header .main-navigation .widget_search input').css('display', 'none');
            $('.site-header .main-navigation .widget_search button').css('display', 'none');
            $('.site-header').next().css({'z-index': '-1', 'position': 'relative'});
            $('body').css({
                'overflow': 'hidden',
                'height': '100vh'
            });
            $('.primary-item').css('height', '100vh');
            $('.mobile-cart > a').css('display', 'none');
            $('.mobile-search > a').css('display', 'none');
            $('.mobile-nav').css('display', 'block');
        }
    });



    $('.close-mobile-nav').click(function() {
        // $('.primary-item-submenu').removeClass('displayed').addClass('undisplayed');

        $(".primary-item-submenu").removeClass("displayed").addClass("undisplayed");
        $('.close-mobile-nav').removeClass('displayed').addClass('undisplayed');
        $('.site-logo').css('display', 'block');
        $('.hamburger').css('display', 'block');
        $('.mobile-nav > ul > li.primary-item > a').css('display', 'block');
        $('.site-header .main-navigation .widget_search input').css('display', 'block');
        $('.site-header .main-navigation .widget_search button').css('display', 'block');
        $('.site-header').next().css({'z-index': '-1', 'position': 'static'});
        $('body').css({
            'overflow': 'scroll',
            'height': 'auto'
        });
        $('.mobile-cart > a').css('display', 'block');
        $('.mobile-search > a').css('display', 'block');
        $('.primary-item').css('height', 'auto');
    });

    $(function() {
        var parent = $('.owl-item');
        var parentWidth = parent.width();
        var image = $('.owl-item img');
        var imageWidth = image.width();
        var posRight = (parentWidth - imageWidth) / 2;

        var prev = $('.owl-prev');
        $(prev).css('left', posRight);
        var next = $('.owl-next');
        $(next).css('right', posRight);

        var lookAll = $('.pogledaj-sve');
        $(lookAll).css('margin-right', posRight + 15);

        var heading = $('.naslovna-carousel-naslov h2');
        $(heading).css('margin-left', posRight);


        // var knaPrev = $('.knjige-na-akciji-container .owl-prev');
        // $(knaPrev).css('left', knaMargin);()
        // var knaNext = $('.knjige-na-akciji-container .owl-next');
        // $(knaNext).css('right', knaMargin);

    });

    $(function() {
        var parent = $('.owl-item img');
        var prev = $('.owl-prev');
        var next = $('.owl-next');
        var windowW = $(window).width();
        if (windowW <= 767) {
            $(prev).css({
                'top': 100,
                'height': 60,
                'width': 30
            });
            $(next).css({
                'top': 100,
                'height': 60,
                'width': 30
            });
        }
    });


    $(function() {
        var windowWidth = $(window).width();
        //console.log(windowWidth);
        var tmp = $('.woocommerce-MyAccount-navigation');
        //console.log(tmp);
        if (windowWidth <= 767) {
            tmp.addClass('undisplayed');
        }
    });

    $('.moj-nalog-kontrolna-tabla-container h1, .wishlist-container .wishlist-naslov h1').click(function() {
        var tmp = $('.woocommerce-MyAccount-navigation');
        tmp.toggleClass('undisplayed displayed');
    });

});

// $('#yith-wcwl-form').load(function(){
//     $('.wishlist-cart .product_type_simple').empty();
//     $('.remove_from_wishlist').empty();
//     $('.owl-carousel-single-image .button.product_type_simple.add_to_cart_button.ajax_add_to_cart').empty();
// });

$(document).ready(function() {
  $('.owl-carousel').each(function(){
      $(this).owlCarousel({
          items: 6,
          margin: 40,
          loop: true,
          responsive: {
              0: {
                  items: 2,
                  nav: true
              },
              700: {
                  items:2,
                  nav: true
              },
              900: {
                  items: 3,
                  nav: true
              },
              1200: {
                  items: 4,
                  nav: true
              },
              1400: {
                  items: 6,
                  nav: true
              }
          }
      });
  });
});


// $(document).ready(function(){
//   $('.product_list_widget').owlCarousel({
//       items: 6,
//       loop: true,
//       margin: 40,
//       responsive: {
//           0: {
//               items: 1,
//               nav: true
//           },
//           700: {
//               items:2,
//               nav: true
//           },
//           900: {
//               items: 3,
//               nav: true
//           },
//           1200: {
//               items: 4,
//               nav: true
//           },
//           1400: {
//               items: 6,
//               nav: true
//           }
//       }
//   });
// });

$(window).load(function() {
  $('.flexslider').flexslider({
    animation: "slide"
  });
});

$(function() {
    var owl = $('.owl-carousel-najnovije');
    owl.owlCarousel();
    // Go to the next item
    $('.owl-next').click(function() {
        owl.trigger('next.owl-carousel-najnovije');
    });
    // Go to the previous item
    $('.owl-prev').click(function() {
        // With optional speed parameter
        // Parameters has to be in square bracket '[]'
        owl.trigger('prev.owl-carousel-najnovije');
    });
});

$(function() {
    var owl = $('.owl-carousel-popularno');
    owl.owlCarousel();
    // Go to the next item
    $('.owl-next').click(function() {
        owl.trigger('next.owl-carousel-popularno');
    });
    // Go to the previous item
    $('.owl-prev').click(function() {
        // With optional speed parameter
        // Parameters has to be in square bracket '[]'
        owl.trigger('prev.owl-carousel-popularno');
    });
});

$(function() {
    var owl = $('.owl-carousel-preporucujemo');
    owl.owlCarousel();
    // Go to the next item
    $('.owl-next').click(function() {
        owl.trigger('next.owl-carousel-preporucujemo');
    });
    // Go to the previous item
    $('.owl-prev').click(function() {
        // With optional speed parameter
        // Parameters has to be in square bracket '[]'
        owl.trigger('prev.owl-carousel-preporucujemo');
    });
});

$(function() {
    var owl = $('.owl-carousel-knjige-na-akciji');
    owl.owlCarousel();
    // Go to the next item
    $('.owl-next').click(function() {
        owl.trigger('next.owl-carousel-knjige-na-akciji');
    });
    // Go to the previous item
    $('.owl-prev').click(function() {
        // With optional speed parameter
        // Parameters has to be in square bracket '[]'
        owl.trigger('prev.owl-carousel-knjige-na-akciji');
    });
});

$(function() {
    var owl = $('.owl-carousel-vec-ste-gledali');
    owl.owlCarousel({
        loop: false
    });
    // // Go to the next item
    // $('.owl-next').click(function() {
    //     owl.trigger('next.owl-carousel-vec-ste-gledali');
    // });
    // // Go to the previous item
    // $('.owl-prev').click(function() {
    //     // With optional speed parameter
    //     // Parameters has to be in square bracket '[]'
    //     owl.trigger('prev.owl-carousel-vec-ste-gledali',);
    // });
});

$(function() {
    $('.site-header .site-nav ul.menu > li > a').click(function() {
        var device = $(window).width();
        if (device <=767) {
            $('.nav-sub-menu').toggle();
        }
    });
});

$(function() {
    var prevent = true;
    $('.open-sub-menu-box a').click(function() {
        var parent = $(this).parent();
        $(parent).children('.sub-menu-box').toggle(function() {
            $('.sub-menu-box').not(this).css('display', 'none');
        });
    });
});

// $(function() {
//     $('body').click(function(e) {
//         e.preventDefault();
//         var target = e.target;
//         var parent = document.querySelectorAll('.sub-menu-box');
//         //console.log(parent);
//         var allChilds = $('.sub-menu-box *');
//         //console.log(allChilds);
//         if (target != parent || target != all) {
//             $('.sub-menu-box').css('display', 'none');
//         }
//     });
// });

$(function() {
    $("body").click(function(e) {
        var elemClasses = Array.from(e.target.parentNode.classList);
        if (elemClasses.length == 0) {
            elemClasses = Array.from(e.target.parentNode.parentNode.classList);
        }
        var prevent = elemClasses.includes('open-sub-menu-box') || elemClasses.includes('open-sub-menu-box-link') || elemClasses.includes('mini-cart-ukupno-cifra') || elemClasses.includes('mini-cart-ukupno') || elemClasses.includes('mini-narudzbina') || elemClasses.includes('mini-narudzbina-price') || elemClasses.includes('woocommerce-Price-amount') || elemClasses.includes('mini-narudzbina-author') || elemClasses.includes('mini-narudzbina-image') || elemClasses.includes('sub-menu-box') || elemClasses.includes('cart-counter') || elemClasses.includes('woocommerce-form-row') || elemClasses.includes('woocommerce-form') || elemClasses.includes('register') || elemClasses.includes('woocommerce-form__label');
        if(!prevent){
               $('.sub-menu-box').css('display', 'none');
        }
        // if (e.target.id == "myDiv" || $(e.target).parents("#myDiv").length) {
        //     alert("Inside div");
        // } else {
        //     alert("Outside div");
        // }
    });
});

// $(window).load(function() {
//     $('#knd-preloader').animate({
//         opacity: 0
//     }, 500, function(){
//         $('#knd-preloader').empty();
//         $('#knd-preloader').remove();
//     });
// });

// $('*').click(function(e) {
//     var target = $('.sub-menu-box');
//     if (e.target != target) {
//         $('.sub-menu-box').css('display', 'none');
//     }
//         e.stopPropagation();
// })
