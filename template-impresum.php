<?php

/* Template Name: Impresum */

get_header();

while ( have_posts() ) :
        the_post(); ?>
        <div class="impresum-container container">
            <h1><?php the_title(); ?></h1>
            <div class="impresum-block">
                <?php the_content(); ?>
            </div>
         </div>
    <?php endwhile;


get_footer();
