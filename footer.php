        <footer class="site-footer">
            <div class="container">
                <div class="footer-images">
                    <div class="logo-left footer-logo">
                        <?php
                        if( is_active_sidebar( 'footer4' ) ) {
                            dynamic_sidebar( 'footer4' );
                        }
                        ?>
                    </div>
                </div>
                <div class="footer-lists">
                    <div class="six-columns footer-list footer-first-list">
                        <?php
                        if( is_active_sidebar( 'footer1' ) ) {
                            dynamic_sidebar( 'footer1' );
                        }
                        ?>
                    </div>
                    <div class="six-columns footer-list footer-second-list">
                        <?php
                        if( is_active_sidebar( 'footer2' ) ) {
                            dynamic_sidebar( 'footer2' );
                        }
                        ?>
                    </div>
                    <div class="four-columns footer-list footer-third-list">
                        <?php
                        if( is_active_sidebar( 'footer3' ) ) {
                            dynamic_sidebar( 'footer3' );
                        }
                        ?>
                    </div>
                    <div class="four-columns footer-list footer-second-list">
                        <?php
                        if( is_active_sidebar( 'footer7' ) ) {
                            dynamic_sidebar( 'footer7' );
                        }
                        ?>
                    </div>
                </div>
            </div>
        </footer>

        <div class="chipcard-footer container">
            <?php
            if( is_active_sidebar( 'footer6' ) ) {
                dynamic_sidebar( 'footer6' );
            }
            ?>
        </div>

        <div class="container">
            <a href='https://www.knjigenadlanu.com/trust/'><img width="250" style="display:table; margin:5px auto;" src='https://verify.etrustmark.rs/cert/image.php'></a>
        </div>

        <div class="copyright-footer">
            <div class="footer-paragraph container">
                <p><?php echo get_theme_mod( 'custom_footer_text' ); ?></p>
            </div>
        </div>

        <?php wp_footer(); ?>

    </body>

</html>
