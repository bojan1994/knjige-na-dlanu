<div class="container vesti-container">
<div class="naslovna-carousel-naslov">
    <a href="<?php echo home_url('/vesti/'); ?>"><h2>Vesti</h2></a>
    <a class="pogledaj-sve" href="<?php echo home_url('/vesti/'); ?>" title="<?php _e('Vesti', 'srkileee-framework'); ?>">pogledaj sve</a>
</div>
<div class="vesti-columns">
<?php global $post;
$args5 = array(
    'post_type' => 'post',
    'posts_per_page' => '4',
    'no_found_rows' => true,
		'cache_results'          => false,
		'update_post_meta_cache' => false,
		'update_post_term_cache' => false,
);
$news = get_posts( $args5 );
foreach ( $news as $post ) :
  setup_postdata( $post ); ?>
  <div class="four-columns vesti-column">
      <a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title(); ?>"><?php the_post_thumbnail('news-image', array('alt' => ''.get_the_title().'', 'title' => ''.get_the_title().'', 'itemprop' => 'image', 'class' => 'lazy img-responsive', 'data-src' => ''.get_the_post_thumbnail_url('', 'medium' ).'', )); ?></a>
      <p class="posts-info"><?php the_time('d.m.Y'); ?>.</p>
      <a href="<?php the_permalink(); ?>"><h3><?php the_title(); ?></h3></a>
      <?php the_excerpt(); ?>
      <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">Više</a>
  </div>
<?php
endforeach;
wp_reset_postdata();
?>
</div>
</div>
