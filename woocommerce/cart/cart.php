<?php
/**
 * Cart Page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/cart/cart.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
?>
<div class="korpa-container container clearfix">
    <h1 class="woocommerce-products-header__title page-title">Korpa</h1>
<?php

wc_print_notices();

do_action( 'woocommerce_before_cart' ); ?>


<div class="cart-narudzbine clearfix">
<form class="woocommerce-cart-form" action="<?php echo esc_url( wc_get_cart_url() ); ?>" method="post">
	<?php do_action( 'woocommerce_before_cart_table' ); ?>

	<table class="shop_table shop_table_responsive cart woocommerce-cart-form__contents" cellspacing="0">
		<thead>
			<tr>
				<th class="product-remove">&nbsp;</th>
				<th class="product-thumbnail"><?php esc_html_e( 'Knjiga', 'woocommerce' ); ?></th>
				<th class="product-price"><?php esc_html_e( 'Cena', 'woocommerce' ); ?></th>
				<th class="product-quantity"><?php esc_html_e( 'Količina', 'woocommerce' ); ?></th>
				<th class="product-subtotal"><?php esc_html_e( 'Ukupno', 'woocommerce' ); ?></th>
			</tr>
		</thead>
		<tbody>
			<?php do_action( 'woocommerce_before_cart_contents' ); ?>

			<?php
			foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
				$_product   = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
				$product_id = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );

				if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
					$product_permalink = apply_filters( 'woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink( $cart_item ) : '', $cart_item, $cart_item_key );
					?>
					<tr class="woocommerce-cart-form__cart-item <?php echo esc_attr( apply_filters( 'woocommerce_cart_item_class', 'cart_item', $cart_item, $cart_item_key ) ); ?>">

						<td class="product-remove">
							<?php
								// @codingStandardsIgnoreLine
								echo apply_filters( 'woocommerce_cart_item_remove_link', sprintf(
									'<a href="%s" class="remove" aria-label="%s" data-product_id="%s" data-product_sku="%s">&times;</a>',
									esc_url( wc_get_cart_remove_url( $cart_item_key ) ),
									__( 'Remove this item', 'woocommerce' ),
									esc_attr( $product_id ),
									esc_attr( $_product->get_sku() )
								), $cart_item_key );
							?>
						</td>

						<td class="product-thumbnail"><?php
						$thumbnail = apply_filters( 'woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $cart_item_key );

						if ( ! $product_permalink ) {
							echo $thumbnail;
						} else {
                            $image = wp_get_attachment_image_src( get_post_thumbnail_id( $_product->get_id() ), 'full' );
                            ?>
                            <a href="<?php the_permalink( $product_id ); ?>"><img src="<?php echo $image[0]; ?>" alt="<?php echo $_product->get_name(); ?>" title="<?php echo $_product->get_name(); ?>"></a>
                            <?php
						}

                        ?>
                        <div class="cart-author">
                            <?php
                            /*$terms = wp_get_post_terms( $_product->get_id(), 'autor' );
                            if( $terms ) {
                                ?>
                                <p class="cart-author-name"><?php echo $terms[0]->name; ?></p>
                                <?php
                            }*/
                            ?>
                            <?php
    						if ( ! $product_permalink ) {
    							echo apply_filters( 'woocommerce_cart_item_name', $_product->get_name(), $cart_item, $cart_item_key ) . '&nbsp;';
    						} else {
    							echo apply_filters( 'woocommerce_cart_item_name', sprintf( '<a href="%s">%s</a>', esc_url( $product_permalink ), $_product->get_name() ), $cart_item, $cart_item_key );
    						}
                            ?>
                        </div>
                        </td>

						<?php

						// Meta data.
						echo wc_get_formatted_cart_item_data( $cart_item );

						// Backorder notification.
						if ( $_product->backorders_require_notification() && $_product->is_on_backorder( $cart_item['quantity'] ) ) {
							echo '<p class="backorder_notification">' . esc_html__( 'Available on backorder', 'woocommerce' ) . '</p>';
						}
						?></td>

						<td class="product-price" data-title="<?php esc_attr_e( 'Price', 'woocommerce' ); ?>">
							<?php
								echo apply_filters( 'woocommerce_cart_item_price', WC()->cart->get_product_price( $_product ), $cart_item, $cart_item_key );
							?>
						</td>

						<td class="product-quantity" data-title="<?php esc_attr_e( 'Quantity', 'woocommerce' ); ?>"><?php
						if ( $_product->is_sold_individually() ) {
							$product_quantity = sprintf( '1 <input type="hidden" name="cart[%s][qty]" value="1" />', $cart_item_key );
						} else {
							$product_quantity = woocommerce_quantity_input( array(
								'input_name'    => "cart[{$cart_item_key}][qty]",
								'input_value'   => $cart_item['quantity'],
								'max_value'     => $_product->get_max_purchase_quantity(),
								'min_value'     => '0',
								'product_name'  => $_product->get_name(),
							), $_product, false );
						}

						echo apply_filters( 'woocommerce_cart_item_quantity', $product_quantity, $cart_item_key, $cart_item );
						?></td>

						<td class="product-subtotal" data-title="<?php esc_attr_e( 'Total', 'woocommerce' ); ?>">
							<?php
								echo apply_filters( 'woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal( $_product, $cart_item['quantity'] ), $cart_item, $cart_item_key );
							?>
						</td>
					</tr>
					<?php
				}
			}
			?>

			<?php do_action( 'woocommerce_cart_contents' ); ?>

			<tr>
				<td colspan="6" class="actions">

					<!-- <?php if ( wc_coupons_enabled() ) { ?>
						<div class="coupon">
							<label for="coupon_code"><?php esc_html_e( 'Coupon:', 'woocommerce' ); ?></label> <input type="text" name="coupon_code" class="input-text" id="coupon_code" value="" placeholder="<?php esc_attr_e( 'Coupon code', 'woocommerce' ); ?>" /> <input type="submit" class="button" name="apply_coupon" value="<?php esc_attr_e( 'Apply coupon', 'woocommerce' ); ?>" />
							<?php do_action( 'woocommerce_cart_coupon' ); ?>
						</div>
					<?php } ?> -->

					<?php do_action( 'woocommerce_cart_actions' ); ?>

					<?php wp_nonce_field( 'woocommerce-cart' ); ?>
				</td>
			</tr>

			<?php do_action( 'woocommerce_after_cart_contents' ); ?>
		</tbody>
	</table>
	<?php do_action( 'woocommerce_after_cart_table' ); ?>

    <button type="submit" class="button" name="update_cart" value="<?php esc_attr_e( 'Update cart', 'woocommerce' ); ?>"><i class="icon-cart"></i><?php esc_html_e( 'Ažuriraj korpu', 'woocommerce' ); ?></button>

</form>

<div class="cart-collaterals clearfix">
	<?php
		/**
		 * Cart collaterals hook.
		 *
		 * @hooked woocommerce_cross_sell_display
		 * @hooked woocommerce_cart_totals - 10
		 */
		do_action( 'woocommerce_cart_collaterals' );
	?>
</div>
</div>

<div class="informacije-o-dostavi">
    <h3>Informacije o dostavi</h3>
    <p class="opis-isporuke">Isporuka se vrši kurirskom službom AKS. Cena isporuke zavisi od težine pošiljke. Više informacija o cenama dostave možete naći u <a href="https://www.knjigenadlanu.com/uslovi/">Uslovima</a>. Rok za dostavu je 3-5 radnih dana.</p>
    <p>do 0.5 kg - <?php echo get_theme_mod( 'custom_shipping_cost' ); ?></p>
    <p>od 0.5 kg - 2 kg - <?php echo get_theme_mod( 'custom_shipping_cost2' ); ?></p>
    <p>od 2 kg - 3 kg - 210 RSD </p>
    <p>od 3 kg - 4 kg - 230 RSD</p>
    <p>od 4 kg - 5 kg - 250 RSD</p>
    <h3 class="dodatne-informacije">Dodatne informacije</h3>
    <p class="dodatne-informacije-paragraph">Za sve dodatne informacije možete pozvati telefon:</p>
    <p class="number">011/41 43 490</p>
    <p class="number">061/65-999-33</p>
    <p class="poruka">Ili poslati poruku na:</p>
    <p class="email"><a href="mailto:office@knjigenadlanu.com" target="_blank">office@knjigenadlanu.com</a></p>
</div>

<div class="clearfix"></div>

<div class="clearfix"></div>

<?php do_action( 'woocommerce_after_cart' ); ?>
</div>

<div class="vec-ste-gledali">
    <div class="vec-ste-gledali-container container">
        <?php
        if ( is_active_sidebar( 'sidebar2' ) ) {
            dynamic_sidebar( 'sidebar2' );
        }
        ?>
    </div>
</div>
