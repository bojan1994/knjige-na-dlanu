<?php
/**
 * The template for displaying product widget entries.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-widget-product.php.
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

global $product;
?>
<div class="vec-ste-gledali-single-item six-columns">
	<?php do_action( 'woocommerce_widget_product_item_start', $args ); ?>

	<a href="<?php echo esc_url( $product->get_permalink() ); ?>">
        <div class="vec-ste-gledali-wrapper">
        	<?php the_post_thumbnail('medium', array('alt' => ''.get_the_title().'', 'title' => ''.get_the_title().'', 'itemprop' => 'image', 'class' => 'lazy', 'data-src' => ''.get_the_post_thumbnail_url('', 'medium' ).'', )); ?>
						<a href="/?add-to-cart=<?php the_ID(); ?>" data-quantity="1" class="button product_type_simple add_to_cart_button ajax_add_to_cart" data-product_id="<?php the_ID(); ?>" data-product_sku="" aria-label="Add <?php the_title(); ?> to your cart" rel="nofollow" title="Dodaj u korpu">Dodaj u korpu</a>
				</div>
		<span class="product-title"><?php echo $product->get_name(); ?></span>

		<?php if ( ! empty( $show_rating ) ) : ?>
			<?php echo wc_get_rating_html( $product->get_average_rating() ); ?>
		<?php endif; ?>

		<?php echo $product->get_price_html(); ?>

		<?php do_action( 'woocommerce_widget_product_item_end', $args ); ?>
	</a>


</div>
