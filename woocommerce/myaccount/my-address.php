<?php
/**
 * My Addresses
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/my-address.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.6.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

$customer_id = get_current_user_id();
$customer = new WC_Customer( get_current_user_id() );

?>

<div class="detalji-adresa">
    <div class="adresa-za-isporuku two-columns">
        <h2>Adresa za isporuku</h2>
        <p><?php echo $customer->get_data()['shipping']['first_name']; ?> <?php echo $customer->get_data()['shipping']['last_name']; ?></p>
        <p><?php echo $customer->get_data()['shipping']['address_1']; ?> <?php echo $customer->get_data()['shipping']['address_2']; ?></p>
        <p><?php echo $customer->get_data()['shipping']['postcode']; ?> <?php echo $customer->get_data()['shipping']['city']; ?></p>
        <p>Srbija</p>
        <div class="telefon-email">
            <?php
            for( $i=0; $i<count($customer->get_meta_data()); $i++ ) {
                if( $customer->get_meta_data()[$i]->get_data()['key'] == 'shipping_broj_telefona' ) {
                    ?>
                    <p>Telefon: <?php echo $customer->get_meta_data()[$i]->get_data()['value']; ?></p>
                    <?php
                }
                if( $customer->get_meta_data()[$i]->get_data()['key'] == 'shipping_e_mail' ) {
                    ?>
                    <p>E-mail: <?php echo $customer->get_meta_data()[$i]->get_data()['value']; ?></p>
                    <?php
                }
            }
            ?>
        </div>
        <a href="<?php echo wc_get_page_permalink( 'myaccount' ); ?>/edit-address/shipping"><i class="icon-edit"></i> Izmenite</a>
    </div>
    <div class="podaci-za-naplatu two-columns">
        <h2>Podaci za naplatu</h2>
        <p><?php echo $customer->get_data()['billing']['first_name']; ?> <?php echo $customer->get_data()['billing']['last_name']; ?> </p>
        <p><?php echo $customer->get_data()['billing']['address_1']; ?> <?php echo $customer->get_data()['billing']['address_2']; ?></p>
        <p><?php echo $customer->get_data()['billing']['postcode']; ?> <?php echo $customer->get_data()['billing']['city']; ?></p>
        <p>Srbija</p>
        <div class="telefon-email">
            <p>Telefon: <?php echo $customer->get_data()['billing']['phone']; ?></p>
            <p>E-mail: <?php echo $customer->get_data()['billing']['email']; ?></p>
        </div>
        <a href="<?php echo wc_get_page_permalink( 'myaccount' ); ?>/edit-address/billing"><i class="icon-edit"></i> Izmenite</a>
    </div>
</div>
