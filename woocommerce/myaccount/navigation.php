<?php
/**
 * My Account navigation
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/navigation.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.6.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

do_action( 'woocommerce_before_account_navigation' );
?>

<nav class="woocommerce-MyAccount-navigation">
	<ul>
        <?php
        $myaccount_page = get_option( 'woocommerce_myaccount_page_id' );
        $myaccount_page_url = get_permalink( $myaccount_page );
        if ( is_user_logged_in() ) {
            ?>
            <li><a href="<?php echo wc_get_endpoint_url( 'orders' ); ?>">Narudžbine</a></li>
            <li><a href="<?php echo wc_get_page_permalink( 'myaccount' ); ?>/lista-zelja/">Lista želja</a></li>
            <li><a href="<?php echo wc_get_endpoint_url( 'edit-address' ); ?>">Adrese</a></li>
            <li><a href="<?php echo wc_get_endpoint_url( 'edit-account' ); ?>">Detalji naloga</a></li>
            <li><a href="<?php echo wc_logout_url( wc_get_page_permalink( 'myaccount' ) ); ?>">Odjavi se</a></li>
            <?php
        } else {
            ?>
            <li><a href="<?php echo wc_get_page_permalink( 'myaccount' ); ?>/lista-zelja/">Lista želja</a></li>
            <?php
        }
        ?>
	</ul>
</nav>

<?php do_action( 'woocommerce_after_account_navigation' ); ?>
