<?php
/**
 * Lost password confirmation text.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/lost-password-confirmation.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.6.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

wc_print_notices();
wc_print_notice( __( 'Password reset email has been sent.', 'woocommerce' ) );
?>

<div class="korpa-container container clearfix">
    <h1 class="woocommerce-products-header__title page-title">Promena lozinke</h1>
    <p><?php echo apply_filters( 'woocommerce_lost_password_message', __( 'Nova lozinka je poslata na vašu adresu, ali može proći nekoliko minuta pre nego što se pojavi u primljenim porukama. Pričekajte najmanje 10 minuta pre nego što pokrenete ponovno postavljanje lozinke.', 'woocommerce' ) ); ?></p>
</div
