<?php
/**
 * View Order
 *
 * Shows the details of a particular order on the account page.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/view-order.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.0.0
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
?>

<?php if ( $notes = $order->get_customer_order_notes() ) : ?>
	<h2><?php _e( 'Order updates', 'woocommerce' ); ?></h2>
	<ol class="woocommerce-OrderUpdates commentlist notes">
		<?php foreach ( $notes as $note ) : ?>
		<li class="woocommerce-OrderUpdate comment note">
			<div class="woocommerce-OrderUpdate-inner comment_container">
				<div class="woocommerce-OrderUpdate-text comment-text">
					<p class="woocommerce-OrderUpdate-meta meta"><?php echo date_i18n( __( 'l jS \o\f F Y, h:ia', 'woocommerce' ), strtotime( $note->comment_date ) ); ?></p>
					<div class="woocommerce-OrderUpdate-description description">
						<?php echo wpautop( wptexturize( $note->comment_content ) ); ?>
					</div>
	  				<div class="clear"></div>
	  			</div>
				<div class="clear"></div>
			</div>
		</li>
		<?php endforeach; ?>
	</ol>
<?php endif; ?>

<?php //do_action( 'woocommerce_view_order', $order_id ); ?>

<div class="view-order-container">
    <div class="view-order-naslov">
        <h1>Detalji narudžbine</h1>
    </div>

<div class="view-order-details">
<table>
    <tr>
        <th colspan="2"><span class="first-span">Narudžbina</span><br> <span>#<?php echo $order->get_order_number(); ?></span></th>
        <th> <span class="first-span">Datum</span> <br><span><?php echo $order->get_date_created()->date( 'd.m.Y' ); ?></span></th>
        <th colspan="2"><span class="first-span">Total</span> <br><span><?php echo $order->get_formatted_order_total(); ?> za <?php echo $order->get_item_count(); ?> stavke</span></th>
    </tr>
    <tr class="opis-porudzbine">
        <td colspan="2">Knjiga</td>
        <td class="cena">Cena</td>
        <td class="kolicina">Količina</td>
        <td class="ukupno">Ukupno</td>
    </tr>
        <?php
        $order_id = $order->get_order_number();
        $order = wc_get_order( $order_id );
        foreach( $order->get_items() as $item_id => $item ) {
            $product = apply_filters( 'woocommerce_order_item_product', $order->get_product_from_item( $item ), $item );
            ?>
            <tr>
                <td class="table-inner-image">
                    <?php
                    $image = wp_get_attachment_image_src( get_post_thumbnail_id( $product->get_id() ), 'full' );
                    ?>
                    <a href="<?php the_permalink( $product->get_id() ); ?>"><img src="<?php echo $image[0]; ?>" alt="<?php echo $product->get_name(); ?>" title="<?php echo $product->get_name(); ?>"></a>
                    <table>
                    <tr><td><a href="<?php the_permalink( $product->get_id() ); ?>"><?php echo $item->get_data()['name']; ?></a></td></tr>
                    <?php
                    $id = $item->get_data()['product_id'];
                    $products = get_posts( array(
                        'post_type' => 'product'
                    ) );
                    /*$terms = get_the_terms($id, 'autor');
                    if( $terms ) {
                        foreach($terms as $term) {
                            ?>
                            <!-- <tr><td><?php echo $term->name; ?></td></tr> -->
                            <?php
                        }
                    }*/
                    ?>
                    </table>
                </td>
                <td colspan="2" class="table-inner-price">
                    <p><?php echo $product->get_data()['price']; ?>.00 RSD</p>
                </td>
                <td class="table-inner-kolicina">
                    <p><?php echo $item->get_data()['quantity']; ?></p>
                </td>
                <td class="table-inner-ukupno">
                    <p><?php echo $order->get_formatted_line_subtotal( $item ); ?></p>
                </td>
            </tr>
            <?php
        }
        ?>
</table>

<div class="detalji-narudzbine-ukupno clearfix">
    <table>
        <tr>
            <td class="vrednost">Vrednost korpe:</td>
            <td class="cena-knjiga"><?php echo $order->get_subtotal_to_display(); ?></td>
        </tr>
        <tr>
            <td class="dostava-text">Dostava:</td>
            <td class="cena-dostava"><?php echo $order->get_shipping_to_display(); ?> <span>Ispod 0.5kg: 175,00 RSD</span></td>
        </tr>
        <tr>
            <td class="ukupno-ukupno">Ukupno:</td>
            <td class="suma-ukupno"><?php echo $order->get_formatted_order_total(); ?></td>
        </tr>
    </table>
</div>
</div>

<div class="view-order-address">
    <div class="adresa-za-isporuku">
        <h2>Adresa za isporuku</h2>
        <p><?php echo $order->get_data()['shipping']['first_name']; ?> <?php echo $order->get_data()['shipping']['last_name']; ?> </p>
        <p><?php echo $order->get_data()['shipping']['address_1']; ?> <?php echo $order->get_data()['shipping']['address_2']; ?></p>
        <p><?php echo $order->get_data()['shipping']['postcode']; ?> <?php echo $order->get_data()['shipping']['city']; ?></p>
        <p>Srbija</p>
        <div class="telefon-email">
            <p>Telefon: <?php echo $order->get_meta_data()[0]->get_data()['value']; ?></p>
            <p>E-mail: <?php echo $order->get_meta_data()[1]->get_data()['value']; ?></p>
        </div>
    </div>
    <div class="adresa-za-naplatu">
        <h2>Podaci za naplatu</h2>
        <p><?php echo $order->get_data()['billing']['first_name']; ?> <?php echo $order->get_data()['billing']['last_name']; ?> </p>
        <p><?php echo $order->get_data()['billing']['address_1']; ?> <?php echo $order->get_data()['billing']['address_2']; ?></p>
        <p><?php echo $order->get_data()['billing']['postcode']; ?> <?php echo $order->get_data()['billing']['city']; ?></p>
        <p>Srbija</p>
        <div class="telefon-email">
            <p>Telefon: <?php echo $order->get_data()['billing']['phone']; ?></p>
            <p>E-mail: <?php echo $order->get_data()['billing']['email']; ?></p>
        </div>
    </div>
</div>
</div>
