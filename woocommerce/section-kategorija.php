<?php
     if (have_posts()) {
         while (have_posts()) {
             the_post(); ?>
                <div class="five-columns kategorije-select-column">
                <div class="owl-carousel-single-image">
                  <a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title(); ?>">
                      <?php the_post_thumbnail('carousel-image', array('alt' => ''.get_the_title().'', 'title' => ''.get_the_title().'', 'itemprop' => 'image', 'class' => 'lazy', 'data-src' => ''.get_the_post_thumbnail_url('', 'carousel-image').'', )); ?>
                    </a>
                  <a href="/?add-to-cart=<?php the_ID(); ?>" data-quantity="1" class="button product_type_simple add_to_cart_button ajax_add_to_cart" data-product_id="<?php the_ID(); ?>" data-product_sku="" aria-label="Add “Pingulingo” to your cart" rel="nofollow" title="Dodaj u korpu">Dodaj u korpu</a>
                  </div>
                </div>
                <?php
         } ?>
                <?php wp_reset_postdata(); ?>
                <?php
     } else {
         ?>
                  <?php _e('Currently there are no articles available', 'srkileee-framework'); ?>
<?php
     } ?>
