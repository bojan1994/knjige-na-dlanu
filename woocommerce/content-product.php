<?php
/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product;
//$product_url = get_page_by_title( 'Product Title', OBJECT, 'product' );

// Ensure visibility
if ( empty( $product ) || ! $product->is_visible() ) {
	return;
}
?>
	<?php
	/**
	 * woocommerce_before_shop_loop_item hook.
	 *
	 * @hooked woocommerce_template_loop_product_link_open - 10
	 */
	do_action( 'woocommerce_before_shop_loop_item' ); // link

	/**
	 * woocommerce_before_shop_loop_item_title hook.
	 *
	 * @hooked woocommerce_show_product_loop_sale_flash - 10
	 * @hooked woocommerce_template_loop_product_thumbnail - 10
	 */
     ?>
	<div class="owl-carousel-single-image">
        <?php the_post_thumbnail('carousel-image', array('alt' => ''.get_the_title().'', 'title' => ''.get_the_title().'', 'itemprop' => 'image', 'class' => 'lazy', 'data-src' => ''.get_the_post_thumbnail_url('', 'carousel-image' ).'', )); ?>

        <?php do_action( 'woocommerce_after_shop_loop_item' ); ?>
    </div>
    <?php

    // $authors = get_the_terms( $post->ID, 'autor' );
    // $separator = ',';
    // $output = '';
    // if ( $authors ) {
    //     foreach( $authors as $author ) {
    //         //$output .= '<div class="naslovna-author-name"><a href=" ' . get_term_link( $author->term_id ) . ' "> ' . $author->name . '</a></div>' . $separator;
    //         $output .= '<div class="naslovna-author-name"> ' . $author->name . '</div>' . $separator;
    //     }
    //     echo trim( $output, $separator );
    // }

	/**
	 * woocommerce_shop_loop_item_title hook.
	 *
	 * @hooked woocommerce_template_loop_product_title - 10
	 */
	?>

	<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><h3 class="naslovna-author-name"><?php the_title(); ?></h3></a>


		<?php do_action( 'woocommerce_after_shop_loop_item_title' ); ?>
