<?php
/**
 * Wishlist page template
 *
 * @author Your Inspiration Themes
 * @package YITH WooCommerce Wishlist
 * @version 2.0.12
 */

if ( ! defined( 'YITH_WCWL' ) ) {
	exit;
} // Exit if accessed directly
?>

<?php do_action( 'yith_wcwl_before_wishlist_form', $wishlist_meta ); ?>

<div class="wishlist-container container">
<div class="wishlist-naslov one-column">
<h1>Moj nalog</h1>
</div>
<div class="wishlist-content">
<nav class="woocommerce-MyAccount-navigation six-columns">
	<ul>
        <?php
        $myaccount_page = get_option( 'woocommerce_myaccount_page_id' );
        $myaccount_page_url = get_permalink( $myaccount_page );
        if ( is_user_logged_in() ) {
            ?>
            <li><a href="<?php echo wc_get_page_permalink( 'myaccount' ); ?>/orders">Narudžbine</a></li>
            <li><a href="<?php echo wc_get_page_permalink( 'myaccount' ); ?>/lista-zelja/">Lista želja</a></li>
            <li><a href="<?php echo wc_get_page_permalink( 'myaccount' ); ?>/edit-address/">Adrese</a></li>
            <li><a href="<?php echo wc_customer_edit_account_url(); ?>">Detalji naloga</a></li>
            <li><a href="<?php echo wp_logout_url( get_permalink( $myaccount_page ) ); ?>">Odjavi se</a></li>
            <?php
        } else {
            ?>
            <li><a href="<?php echo wc_get_page_permalink( 'myaccount' ); ?>/lista-zelja/">Lista želja</a></li>
            <?php
        }
        ?>
	</ul>
</nav>

<div class="wishlist-content-inner">
<form id="yith-wcwl-form" action="<?php echo $form_action ?>" method="post" class="woocommerce">

    <?php wp_nonce_field( 'yith-wcwl-form', 'yith_wcwl_form_nonce' ) ?>

    <!-- TITLE -->
    <?php
    do_action( 'yith_wcwl_before_wishlist_title', $wishlist_meta );

    if( ! empty( $page_title ) ) :
    ?>
        <div class="wishlist-title <?php echo ( $is_custom_list ) ? 'wishlist-title-with-form' : ''?>">
            <div class="wishlist-content-inner-naslov one-column"> <?php echo apply_filters( 'yith_wcwl_wishlist_title', '<h2>' . $page_title . '</h2>' ); ?></div>
            <?php if( $is_custom_list ): ?>
                <a class="btn button show-title-form">
                    <?php echo apply_filters( 'yith_wcwl_edit_title_icon', '<i class="fa fa-pencil"></i>' )?>
                    <?php _e( 'Edit title', 'yith-woocommerce-wishlist' ) ?>
                </a>
            <?php endif; ?>
        </div>
        <?php if( $is_custom_list ): ?>
            <div class="hidden-title-form">
                <input type="text" value="<?php echo $page_title ?>" name="wishlist_name"/>
                <button>
                    <?php echo apply_filters( 'yith_wcwl_save_wishlist_title_icon', '<i class="fa fa-check"></i>' )?>
                    <?php _e( 'Save', 'yith-woocommerce-wishlist' )?>
                </button>
                <a class="hide-title-form btn button">
                    <?php echo apply_filters( 'yith_wcwl_cancel_wishlist_title_icon', '<i class="fa fa-remove"></i>' )?>
                    <?php _e( 'Cancel', 'yith-woocommerce-wishlist' )?>
                </a>
            </div>
        <?php endif; ?>
    <?php
    endif;

     do_action( 'yith_wcwl_before_wishlist', $wishlist_meta ); ?>

    <!-- WISHLIST TABLE -->

	<table class="shop_table cart wishlist_table" data-pagination="<?php echo esc_attr( $pagination )?>" data-per-page="<?php echo esc_attr( $per_page )?>" data-page="<?php echo esc_attr( $current_page )?>" data-id="<?php echo $wishlist_id ?>" data-token="<?php echo $wishlist_token ?>">

	    <?php $column_count = 2; ?>
        <?php
        if( count( $wishlist_items ) > 0 ) :
	        $added_items = array();
            foreach( $wishlist_items as $item ) :
                global $product;

	            $item['prod_id'] = yit_wpml_object_id ( $item['prod_id'], 'product', true );

	            if( in_array( $item['prod_id'], $added_items ) ){
		            continue;
	            }

	            $added_items[] = $item['prod_id'];
	            $product = wc_get_product( $item['prod_id'] );
	            $availability = $product->get_availability();
	            $stock_status = $availability['class'];

                if( $product && $product->exists() ) :
	                ?>
                    <tr id="yith-wcwl-row-<?php echo $item['prod_id'] ?>" data-row-id="<?php echo $item['prod_id'] ?>">
                        <td>
                            <div class="wishlist-content-inner-image">
                                <?php if( $show_cb ) : ?>
    			                    <input type="checkbox" value="<?php echo esc_attr( $item['prod_id'] ) ?>" name="add_to_cart[]" <?php echo ( ! $product->is_type( 'simple' ) ) ? 'disabled="disabled"' : '' ?>/>
        	                    <?php endif; ?>

                                <?php if( $is_user_owner ): ?>
                                    <div>
                                        <a href="<?php echo esc_url( add_query_arg( 'remove_from_wishlist', $item['prod_id'] ) ) ?>" class="remove remove_from_wishlist" title="<?php _e( 'Uklonite proizvod sa liste želja', 'yith-woocommerce-wishlist' ) ?>">&times;</a>
                                    </div>
                                <?php endif; ?>

                                    <a href="<?php echo esc_url( get_permalink( apply_filters( 'woocommerce_in_cart_product', $item['prod_id'] ) ) ) ?>">
                                        <?php
                                        $image = wp_get_attachment_image_src( get_post_thumbnail_id( $product->get_id() ), 'carousel-image' );
                                        ?>
                                        <img src="<?php echo $image[0]; ?>" alt="<?php echo $product->get_name(); ?>" title="<?php echo $product->get_name(); ?>">
                                    </a>
                                    <?php if( $show_last_column ): ?>
            	                        <!-- Date added -->
            	                        <?php
            	                        if( $show_dateadded && isset( $item['dateadded'] ) ):
            								echo '<span class="dateadded">' . sprintf( __( 'Added on : %s', 'yith-woocommerce-wishlist' ), date_i18n( get_option( 'date_format' ), strtotime( $item['dateadded'] ) ) ) . '</span>';
            	                        endif;
            	                        ?>

            	                        <!-- Add to cart button -->
                                        <?php if( $show_add_to_cart && isset( $stock_status ) && $stock_status != 'out-of-stock' ): ?>
                                        <div class="wishlist-cart"><?php woocommerce_template_loop_add_to_cart(); ?></div>
                                        <?php endif ?>

            	                        <!-- Remove from wishlist -->
            	                        <?php if( $is_user_owner && $repeat_remove_button ): ?>
                                            <a href="<?php echo esc_url( add_query_arg( 'remove_from_wishlist', $item['prod_id'] ) ) ?>" class="remove_from_wishlist button" title="<?php _e( 'Remove this product', 'yith-woocommerce-wishlist' ) ?>"><?php _e( 'Remove', 'yith-woocommerce-wishlist' ) ?></a>
                                        <?php endif; ?>
                            </div>

                                <?php
                                // $authors = get_the_terms( $item['prod_id'], 'autor' );
                                // $separator = ',';
                                // $output = '';
                                // if ( $authors ) {
                                //   foreach( $authors as $author ) {
                                //       $output .= '<div class="naslovna-author-name"> ' . $author->name . '</div>' . $separator;
                                //   }
                                //   echo trim( $output, $separator );
                                // }
                                ?>

                                <a class="wish-list-item-link" href="<?php echo esc_url( get_permalink( apply_filters( 'woocommerce_in_cart_product', $item['prod_id'] ) ) ) ?>"><?php echo apply_filters( 'woocommerce_in_cartproduct_obj_title', $product->get_title(), $product ) ?></a>
                                <?php do_action( 'yith_wcwl_table_after_product_name', $item ); ?>

                                <div class="wish-list-item-price">
                                    <?php if( $show_price ) : ?>
                                        <?php
                                        $base_product = $product->is_type( 'variable' ) ? $product->get_variation_regular_price( 'max' ) : $product->get_price();
                                        echo $base_product ? $product->get_price_html() : apply_filters( 'yith_free_text', __( 'Free!', 'yith-woocommerce-wishlist' ) );
                                        ?>
                                    <?php endif; ?>
                                </div>

                        </td>
	                <?php endif; ?>
                    </tr>
                <?php
                endif;
            endforeach;
        else: ?>
            <tr>
                <td colspan="<?php echo esc_attr( $column_count ) ?>" class="wishlist-empty"><?php echo apply_filters( 'yith_wcwl_no_product_to_remove_message', __( 'Lista želja je prazna.', 'yith-woocommerce-wishlist' ) ) ?></td>
            </tr>
        <?php
    endif; ?>
        <?php if( ! empty( $page_links ) ) : ?>
            <tr class="pagination-row">
                <td colspan="<?php echo esc_attr( $column_count ) ?>"><?php echo $page_links ?></td>
            </tr>
        <?php endif ?>

    </table>

    <?php wp_nonce_field( 'yith_wcwl_edit_wishlist_action', 'yith_wcwl_edit_wishlist' ); ?>

    <?php if( ! $is_default ): ?>
        <input type="hidden" value="<?php echo $wishlist_token ?>" name="wishlist_id" id="wishlist_id">
    <?php endif; ?>

    <?php do_action( 'yith_wcwl_after_wishlist', $wishlist_meta ); ?>

</form>
</div>
</div>
<?php do_action( 'yith_wcwl_after_wishlist_form', $wishlist_meta ); ?>

<?php if( $show_ask_estimate_button && ( ! is_user_logged_in() || $additional_info ) ): ?>
	<div id="ask_an_estimate_popup">
		<form action="<?php echo $ask_estimate_url ?>" method="post" class="wishlist-ask-an-estimate-popup">
			<?php if( ! is_user_logged_in() ): ?>
				<label for="reply_email"><?php echo apply_filters( 'yith_wcwl_ask_estimate_reply_mail_label', __( 'Your email', 'yith-woocommerce-wishlist' ) ) ?></label>
				<input type="email" value="" name="reply_email" id="reply_email">
			<?php endif; ?>
			<?php if( ! empty( $additional_info_label ) ):?>
				<label for="additional_notes"><?php echo esc_html( $additional_info_label ) ?></label>
			<?php endif; ?>
			<textarea id="additional_notes" name="additional_notes"></textarea>

			<button class="btn button ask-an-estimate-button ask-an-estimate-button-popup" >
				<?php echo apply_filters( 'yith_wcwl_ask_an_estimate_icon', '<i class="fa fa-shopping-cart"></i>' )?>
				<?php echo apply_filters( 'yith_wcwl_ask_an_estimate_text', __( 'Ask for an estimate', 'yith-woocommerce-wishlist' ) ) ?>
			</button>
		</form>
	</div>
<?php endif; ?>
</div>
