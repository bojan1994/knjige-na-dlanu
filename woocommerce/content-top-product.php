<?php
/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product;

// Ensure visibility
if ( empty( $product ) || ! $product->is_visible() ) {
	return;
}
do_action( 'woocommerce_before_shop_loop_item' ); // link
the_post_thumbnail('carousel-image', array('alt' => ''.get_the_title().'', 'title' => ''.get_the_title().'', 'itemprop' => 'image', 'class' => 'lazy', 'data-src' => ''.get_the_post_thumbnail_url('', 'carousel-image' ).'', ));
// $authors = get_the_terms( $post->ID, 'autor' );
// $separator = ',';
// $output = '';
?>
<div class="top-knjige-single-inner">
    <?php
    // if ( $authors ) {
    //     foreach( $authors as $author ) {
    //         $output .= '<div class="top-knjige-autor">'.$author->name.'</div>';
    //     }
    //     echo trim( $output, $separator );
    // }
    do_action( 'woocommerce_shop_loop_item_title' ); // naslov
    ?>
</div>
<?php
?>
