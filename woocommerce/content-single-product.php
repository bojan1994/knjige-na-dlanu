<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Hook Woocommerce_before_single_product.
 *
 * @hooked wc_print_notices - 10
 */
?>
<div class="container">
    <?php do_action( 'woocommerce_before_single_product' ); ?>
</div>
<?php

if ( post_password_required() ) {
	echo get_the_password_form(); // WPCS: XSS ok.
	return;
}
?>
<div id="product-<?php the_ID(); ?>" <?php post_class(); ?>>
    <?php
    global $product;
    if ( ! wp_is_mobile() ) {
        ?>
        <div class="container single-product-container">
            <div class="six-columns single-product-kategorije-menu">
                <h4 class="kategorije-heading">Kategorije</h4>
                <?php
                //global $product;
                //$term_list = wp_get_post_terms($product->get_ID(),'product_cat',array('fields'=>'ids'));
                //$cat_id = (int)$term_list[0];
                //echo get_term_link ($cat_id, 'product_cat');
                // $cat = get_queried_object();
                // $catID = $cat->term_id;
                // $cat_args = array(
                //     'orderby'    => 'name',
                //     'order'      => 'asc',
                //     'hide_empty' => false,
                //     'parent' => 41,
                // );
                // $product_categories = get_terms( 'product_cat', $cat_args );
                // echo '<ul>';
                // if( ! empty( $product_categories ) ){
                //     foreach ( $product_categories as $key => $category ) {
                //         echo '<li><a href="' . get_term_link( $category ) . '">' . $category->name . ' (' . $category->count . ')</a></li>';
                //     }
                // }
                $terms = wp_get_post_terms( $product->get_id(), 'product_cat', array('exclude'=>array( 5162 ) ) );
                echo '<ul>';
                foreach($terms as $term) {
                    echo '<li><a href="' . get_term_link( $term ) . '"><span><h2>' . $term->name . ' </h2> (' . $term->count . ')</span></a></li>';
                }
                echo '</ul>';
                ?>
            </div>

        		<?php
        			/**
        			 * Hook: Woocommerce_single_product_summary.
        			 *
        			 * @hooked woocommerce_template_single_title - 5
        			 * @hooked woocommerce_template_single_rating - 10
        			 * @hooked woocommerce_template_single_price - 10
        			 * @hooked woocommerce_template_single_excerpt - 20
        			 * @hooked woocommerce_template_single_add_to_cart - 30
        			 * @hooked woocommerce_template_single_meta - 40
        			 * @hooked woocommerce_template_single_sharing - 50
        			 * @hooked WC_Structured_Data::generate_product_data() - 60
        			 */
                     ?>
                     <div class="single-product-knjiga two-columns">
                        <div class="single-product-knjiga-top">
                            <div class="single-product-knjiga-top-image">
                                <?php
                                the_post_thumbnail('medium', array('alt' => ''.get_the_title().'', 'title' => ''.get_the_title().'', 'itemprop' => 'image', 'class' => 'lazy', 'data-src' => ''.get_the_post_thumbnail_url('', 'medium' ).'', ));
                                echo do_shortcode( '[wc_quick_buy_link]' );
                                ?>
                            </div>
                            <div class="single-product-knjiga-top-description">
                                <?php echo woocommerce_template_single_title();
                                // $authors = get_the_terms( $post->ID, 'autor' );
                                // $separator = ',';
                                // $output = '';
                                // if ( $authors ) {
                                //     foreach( $authors as $author ) {
                                //         $output .= '<div class="naslovna-author-name"> ' . $author->name . '</div>' . $separator;
                                //     }
                                //     echo trim( $output, $separator );
                                // }
                                echo do_shortcode( '[yith_wcwl_add_to_wishlist]' );
                                echo woocommerce_template_single_excerpt(); ?>
                                <?php
                                $author = $product->get_attribute( 'pa_izdavac' );
                                if( $author ) {
                                    ?>
                                    <p class="izdavac">Izdavač: <?php echo $author; ?></p>
                                    <?php
                                }
                                ?>
                            </div>
                        </div>
                        <?php
                        $content = get_the_content();
                        if( $content ) {
                            ?>
                            <div class="single-product-knjiga-bottom">
                                <h2>O knjizi</h2>
                                <?php echo $content; ?>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                    <div class="single-product-price three-columns">
                         <h3>Cena</h3>
                         <?php echo woocommerce_template_single_price(); ?>
                         <?php do_action( 'woocommerce_after_shop_loop_item' ); ?>
                         <h3>Informacije o dostavi</h3>
                         <p class="opis-isporuke">Isporuka se vrši kurirskom službom AKS. Cena isporuke zavisi od težine pošiljke. Više informacija o cenama dostave možete naći u <a href="https://www.knjigenadlanu.com/uslovi/">Uslovima</a>. Rok za dostavu je 3-5 radnih dana.</p>
                         <p>do 0.5 kg - <?php echo get_theme_mod( 'custom_shipping_cost' ); ?></p>
                         <p>od 0.5 kg - 2 kg - <?php echo get_theme_mod( 'custom_shipping_cost2' ); ?></p>
                         <p>od 2 kg - 3 kg - 210 RSD </p>
                         <p>od 3 kg - 4 kg - 230 RSD</p>
                         <p>od 4 kg - 5 kg - 250 RSD</p>
                         <h3 class="dodatne-informacije">Dodatne informacije</h3>
                         <p class="dodatne-informacije-paragraph">Za sve dodatne informacije možete pozvati telefon:</p>
                         <p class="number">011/41 43 490</p>
                         <p class="number">061/65-999-33</p>
                         <p class="poruka">Ili poslati poruku na:</p>
                         <p class="email"><a href="mailto:office@knjigenadlanu.com" target="_blank">office@knjigenadlanu.com</a></p>
                     </div>

        </div>
        <?php
    } else {
        ?>
        <div class="container single-product-container-mobile">
            <div class="single-product-knjiga-mobile">
                <?php echo woocommerce_template_single_title(); ?>
                <?php the_post_thumbnail('medium', array('alt' => ''.get_the_title().'', 'title' => ''.get_the_title().'', 'itemprop' => 'image', 'class' => 'lazy', 'data-src' => ''.get_the_post_thumbnail_url('', 'medium' ).'', )); ?>
                <div class="quick_buy_container quick_buy_<?php the_ID(); ?>_container" id="quick_buy_<?php the_ID(); ?>_container">
					<a href="/?add-to-cart=<?php the_ID(); ?>&amp;quantity&amp;quick_buy=1" id="quick_buy_<?php the_ID(); ?>_button" name="" data-product-type="simple" data-product-id="<?php the_ID(); ?>" class="wcqb_button wc_quick_buy_button quick_buy_button quick_buy_simple quick_buy_<?php the_ID(); ?>_button quick_buy_<?php the_ID(); ?>">Kupi odmah</a>
                </div>
                <?php do_action( 'woocommerce_after_shop_loop_item' ); ?>
            </div>
			<div class="more-details">
	            <div class="single-product-price-mobile">
	                <?php echo woocommerce_template_single_price(); ?>
	            </div>
	            <div class="single-product-wishlist-mobile">
	                <?php
	                echo do_shortcode( '[yith_wcwl_add_to_wishlist]' );
	                ?>
	            </div>
	            <div class="single-product-description-mobile">
	                <?php echo woocommerce_template_single_excerpt(); ?>
	            </div>
			</div>
            <div class="single-product-info-mobile">
                <h3>Informacije o dostavi</h3>
                <p class="opis-isporuke">Isporuka se vrši poštom. Cena isporuke zavisi od težine pošiljke, po sledećem cenovniku Pošte Srbije: </p>
                <p>do 0.5 kg - <?php echo get_theme_mod( 'custom_shipping_cost' ); ?></p>
                <p>od 0.5 kg - 2 kg - <?php echo get_theme_mod( 'custom_shipping_cost2' ); ?></p>
				<h3 class="dodatne-informacije">Dodatne informacije</h3>
	            <p class="dodatne-informacije-paragraph">Za sve dodatne informacije možete pozvati telefon:</p>
	            <p class="number">+381 11 24 20 677</p>
	            <p class="number">061/65-999-33</p>
	            <p class="poruka">Ili poslati poruku na:</p>
	            <p class="email"><a href="mailto:office@knjigenadlanu.com" target="_blank">office@knjigenadlanu.com</a></p>
            </div>
            <div class="single-product-cat-mobile">
                <h4 class="kategorije-heading">Kategorije</h4>
                <?php
                $terms = wp_get_post_terms( $product->get_id(), 'product_cat', array('exclude'=>array( 5162 )) );
                ?>
                <ul class="undisplayed">
                    <?php
                    foreach( $terms as $term ) {
                        ?>
                        <li><a href="<?php echo get_term_link( $term ); ?>"><span><h2><?php echo $term->name; ?></h2> (<?php echo $term->count; ?>)</span></a></li>
                        <?php
                    }
                    ?>
                </ul>
            </div>
            <div class="single-product-full-description-mobile">
                <h2>O knjizi</h2>
                <?php the_content(); ?>
            </div>
        </div>
        <?php
    }
    ?>
    <div class="vec-ste-gledali">
        <div class="vec-ste-gledali-container container">
            <?php
            if ( is_active_sidebar( 'sidebar2' ) ) {
                dynamic_sidebar( 'sidebar2' );
            }
            ?>
        </div>
    </div>
</div>
<?php do_action( 'woocommerce_after_single_product' ); ?>
