<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

get_header( 'shop' );

/**
 * Hook: woocommerce_before_main_content.
 *
 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
 * @hooked woocommerce_breadcrumb - 20
 * @hooked WC_Structured_Data::generate_website_data() - 30
 */


?>
<div class="container kategorije-container">
    <div class="six-columns kategorije-menu">
        <?php
        $cat = get_queried_object();
        $catID = $cat->term_id;
        $cat_args = array(
            'orderby'    => 'name',
            'order'      => 'asc',
            'hide_empty' => true,
            'parent' => $catID,
        );
        $product_categories = get_terms( 'product_cat', $cat_args );
        $cat2 = get_queried_object();
        $catID2 = $cat2->term_id;
        $cat_args2 = array(
            'orderby'    => 'name',
            'order'      => 'asc',
            'hide_empty' => true,
            'child_of' => get_term( $catID2, 'product_cat' )->parent,
            'exclude' => array( 5162 ),
        );
        $product_categories2 = get_terms( 'product_cat', $cat_args2 );
        echo '<h2 class="kategorije-heading">Kategorije</h2>';
        echo '<ul>';
        if( ! empty( $product_categories ) ) {
            foreach ( $product_categories as $key => $category ) {
                echo '<li><a href="' . get_term_link( $category ) . '"><span><h2>' . $category->name . ' </h2> (' . $category->count . ')</span></a></li>';
            }
        } else {
            foreach ( $product_categories2 as $key => $category ) {
                echo '<li><a href="' . get_term_link( $category ) . '"><span><h2>' . $category->name . ' </h2> (' . $category->count . ')</span></a></li>';
            }
        }
        echo '</ul>';
        ?>
    </div>

    <div class="kategorije-select">
        <div class="kategorije-naslov"><header class="woocommerce-products-header one-column">
	<?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>
		<h1 class="woocommerce-products-header__title page-title"><?php woocommerce_page_title(); ?></h1>
        <?php do_action( 'woocommerce_before_main_content' ); ?>
        <?php
        if( ! wp_is_mobile() ) {
            ?>
            <div class="sort-form clearfix">
                <p><?php do_action( 'woocommerce_before_shop_loop' ); ?><span class="sort">Sortiraj po: </span></p>
            </div>
            <?php
        } else {
            ?>
            <p><span>Sortiraj po: </span><?php do_action( 'woocommerce_before_shop_loop' ); ?></p>
            <?php
        }
        ?>
	<?php endif; ?>

	<?php
	/**
	 * Hook: woocommerce_archive_description.
	 *
	 * @hooked woocommerce_taxonomy_archive_description - 10
	 * @hooked woocommerce_product_archive_description - 10
	 */
	do_action( 'woocommerce_archive_description' );
	?>
</header>
</div>



<?php
     if (have_posts()) {
         while (have_posts()) {
             the_post(); ?>
                <div class="five-columns kategorije-select-column">
									<div>
	                  <div class="owl-carousel-single-image">
	                    <a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title(); ?>">
	                        <?php the_post_thumbnail('medium', array('alt' => ''.get_the_title().'', 'title' => ''.get_the_title().'', 'itemprop' => 'image', 'class' => 'lazy', 'data-src' => ''.get_the_post_thumbnail_url('', 'medium' ).'', )); ?>
	                      </a>
	                    <a href="/?add-to-cart=<?php the_ID(); ?>" data-quantity="1" class="button product_type_simple add_to_cart_button ajax_add_to_cart" data-product_id="<?php the_ID(); ?>" data-product_sku="" aria-label="Add <?php the_title(); ?> to your cart" rel="nofollow" title="Dodaj u korpu">Dodaj u korpu</a>

	                    </div>
	                    <a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title(); ?>"><h3 class="naslovna-author-name"><?php the_title(); ?></h3></a>
	                    <?php global $woocommerce;
	                                  $currency = get_woocommerce_currency_symbol();
	                                  $price = get_post_meta( get_the_ID(), '_regular_price', true);
	                                  $sale = get_post_meta( get_the_ID(), '_sale_price', true);
	                                  ?>

	                                  <?php if($sale) : ?>
	                                  <span class="price"><del><span class="woocommerce-Price-amount amount"><?php echo $price; ?>&nbsp;<span class="woocommerce-Price-currencySymbol"><?php echo $currency; ?></span></span></del> <ins><span class="woocommerce-Price-amount amount"><?php echo $sale; ?>&nbsp;<span class="woocommerce-Price-currencySymbol"><?php echo $currency; ?></span></span></ins> <small class="woocommerce-price-suffix">sa PDV-om</small></span>
	                                  <?php elseif($price) : ?>
	                                  <span class="price"><span class="woocommerce-Price-amount amount"><?php echo $price; ?>&nbsp;<span class="woocommerce-Price-currencySymbol"><?php echo $currency; ?></span></span> <small class="woocommerce-price-suffix">sa PDV-om</small></span>
	                                  <?php endif; ?>


	                  </div>
                </div>
                <?php
         } ?>
				 <nav class="navigation" role="navigation">
						<?php global $wp_query;
											$big = 999999999; // need an unlikely integer
											echo paginate_links(array(
													'base' => str_replace($big, '%#%', esc_url(get_pagenum_link($big))),
													'format' => '?paged=%#%',
													'current' => max(1, get_query_var('paged')),
													'total' => $wp_query->max_num_pages
											)); ?>
					</nav>

                <?php
     } else {
         ?>
                  <?php _e('Currently there are no articles available', 'srkileee-framework'); ?>
<?php } ?>

</div></div>

<?php get_template_part('section', 'akcija'); ?>

<?php

/**
 * Hook: woocommerce_after_main_content.
 *
 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
 */
//do_action( 'woocommerce_after_main_content' );

/**
 * Hook: woocommerce_sidebar.
 *
 * @hooked woocommerce_get_sidebar - 10
 */
do_action( 'woocommerce_sidebar' );

get_footer( 'shop' );
