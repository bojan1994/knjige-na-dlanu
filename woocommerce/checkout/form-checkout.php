<div class="checkout-container container">
<?php
/**
 * Checkout Form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/form-checkout.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

?>

<h1>Plaćanje</h1>

<?php

wc_print_notices();

do_action( 'woocommerce_before_checkout_form', $checkout );

// If checkout registration is disabled and not logged in, the user cannot checkout
if ( ! $checkout->is_registration_enabled() && $checkout->is_registration_required() && ! is_user_logged_in() ) {
	echo apply_filters( 'woocommerce_checkout_must_be_logged_in_message', __( 'You must be logged in to checkout.', 'woocommerce' ) );
	return;
}

?>

<form name="checkout" method="post" class="checkout woocommerce-checkout" action="<?php echo esc_url( wc_get_checkout_url() ); ?>" enctype="multipart/form-data">

	<?php if ( $checkout->get_checkout_fields() ) : ?>

		<?php do_action( 'woocommerce_checkout_before_customer_details' ); ?>

		<div class="col2-set" id="customer_details">
			<div class="col-1">
				<?php do_action( 'woocommerce_checkout_billing' ); ?>
			</div>

			<div class="col-2">
				<?php do_action( 'woocommerce_checkout_shipping' ); ?>
			</div>
		</div>

		<?php do_action( 'woocommerce_checkout_after_customer_details' ); ?>

	<?php endif; ?>

	<?php do_action( 'woocommerce_checkout_before_order_review' ); ?>

	<div id="order_review" class="woocommerce-checkout-review-order">
        <table class="shop_table shop_table_responsive cart woocommerce-cart-form__contents" cellspacing="0">
            <thead>
                <tr>
                    <th class="product-thumbnail"><?php esc_html_e( 'Knjiga', 'woocommerce' ); ?></th>
                    <th class="product-price"><?php esc_html_e( 'Cena', 'woocommerce' ); ?></th>
                    <th class="product-quantity"><?php esc_html_e( 'Količina', 'woocommerce' ); ?></th>
                    <th class="product-subtotal"><?php esc_html_e( 'Ukupno', 'woocommerce' ); ?></th>
                </tr>
            </thead>
            <tbody>
                <?php do_action( 'woocommerce_before_cart_contents' ); ?>

                <?php
                foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
                    $_product   = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
                    $product_id = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );

                    if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
                        $product_permalink = apply_filters( 'woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink( $cart_item ) : '', $cart_item, $cart_item_key );
                        ?>
                        <tr class="woocommerce-cart-form__cart-item <?php echo esc_attr( apply_filters( 'woocommerce_cart_item_class', 'cart_item', $cart_item, $cart_item_key ) ); ?>">

                            <td class="product-thumbnail">
                                <div class="cart-author">
                                    <?php
                                    /*$terms = wp_get_post_terms( $_product->get_id(), 'autor' );
                                    if( $terms ) {
                                        ?>
                                        <p class="cart-author-name"><?php echo $terms[0]->name; ?> -->
                                        <?php
                                    }*/
                                    if ( ! $product_permalink ) {
                                        echo apply_filters( 'woocommerce_cart_item_name', $_product->get_name(), $cart_item, $cart_item_key ) . '&nbsp;';
                                    } else {
                                        echo apply_filters( 'woocommerce_cart_item_name', sprintf( '<a href="%s">%s</a>', esc_url( $product_permalink ), $_product->get_name() ), $cart_item, $cart_item_key );
                                    }
                                    ?>
                                </div>
                            </td>

                            <?php

                            // Meta data.
                            echo wc_get_formatted_cart_item_data( $cart_item );

                            // Backorder notification.
                            if ( $_product->backorders_require_notification() && $_product->is_on_backorder( $cart_item['quantity'] ) ) {
                                echo '<p class="backorder_notification">' . esc_html__( 'Available on backorder', 'woocommerce' ) . '</p>';
                            }
                            ?></td>

                            <td class="product-price" data-title="<?php esc_attr_e( 'Price', 'woocommerce' ); ?>">
                                <?php
                                    echo apply_filters( 'woocommerce_cart_item_price', WC()->cart->get_product_price( $_product ), $cart_item, $cart_item_key );
                                ?>
                            </td>

                            <td class="product-quantity" data-title="<?php esc_attr_e( 'Quantity', 'woocommerce' ); ?>"><?php
                            $targeted_id = $product_id;
                            foreach ( WC()->cart->get_cart() as $cart_item ) {
                                if($cart_item['product_id'] == $targeted_id ){
                                    $qty =  $cart_item['quantity'];
                                    break;
                                }
                            }
                            if( ! empty( $qty ) ) {
                                echo $qty;
                            }
                            ?></td>

                            <td class="product-subtotal" data-title="<?php esc_attr_e( 'Total', 'woocommerce' ); ?>">
                                <?php
                                    echo apply_filters( 'woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal( $_product, $cart_item['quantity'] ), $cart_item, $cart_item_key );
                                ?>
                            </td>
                        </tr>
                        <?php
                    }
                }
                ?>

                <?php do_action( 'woocommerce_cart_contents' ); ?>
                <?php
                global $woocommerce;
                ?>

                <tr class="order-total">
                    <th colspan="3"><?php _e( 'Vrednost korpe:', 'woocommerce' ); ?></th>
                    <td><?php echo $woocommerce->cart->get_cart_total(); ?> <span>sa PDV-om</span></td>
                </tr>

                <tr class="shipping">
                    <th colspan="3"><?php _e( 'Dostava:', 'woocommerce' ); ?></th>
                    <td><?php echo WC()->cart->get_shipping_total(); ?> RSD <span>ispod 0.5kg 175din</span></td>
                </tr>

                <tr class="total">
                    <th colspan="3"><?php _e( 'Ukupno:', 'woocommerce' ); ?></th>
                    <td><p><?php echo $woocommerce->cart->total; ?> RSD</p></td>
                </tr>

                <?php do_action( 'woocommerce_after_cart_contents' ); ?>
            </tbody>
        </table>
		<?php do_action( 'woocommerce_checkout_order_review' ); ?>
	</div>

	<?php do_action( 'woocommerce_checkout_after_order_review' ); ?>

</form>

</div><!--end of checkout-container-->

<div class="vec-ste-gledali">
    <div class="vec-ste-gledali-container container">
        <?php
        if ( is_active_sidebar( 'sidebar2' ) ) {
            dynamic_sidebar( 'sidebar2' );
        }
        ?>
    </div>
</div>

<?php do_action( 'woocommerce_after_checkout_form', $checkout ); ?>
