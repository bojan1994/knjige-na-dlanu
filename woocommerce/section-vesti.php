<div class="container vesti-container">
<div class="naslovna-carousel-naslov">
    <h2>Vesti</h2>
    <a class="pogledaj-sve" href="<?php echo home_url('/vesti/'); ?>" title="<?php _e('Vesti', 'srkileee-framework'); ?>">pogledaj sve</a>
</div>
<?php

$args = array(
    'post_type' => 'post',
    'posts_per_page' => '4',
);
$vesti_query = new WP_Query( $args ); ?>
<div class="vesti-columns">
<?php if ( $vesti_query->have_posts() ) :
    while ( $vesti_query->have_posts() ) :
        $vesti_query->the_post(); ?>
        <div class="four-columns vesti-column">
            <a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title(); ?>">
              <?php the_post_thumbnail('medium', array('alt' => ''.get_the_title().'', 'title' => ''.get_the_title().'', 'itemprop' => 'image', 'class' => 'lazy owl-lazy img-responsive', 'data-src' => ''.get_the_post_thumbnail_url('', 'medium' ).'', )); ?>
            </a>
            <p class="posts-info"><?php the_time('d.m.Y'); ?>.</p>
            <h3><?php the_title(); ?></h3>
            <?php the_excerpt(); ?>
            <a href="<?php the_permalink(); ?>">Više</a>
        </div>
    <?php endwhile;
    wp_reset_postdata();
endif; ?>
</div>
</div>
