<?php

if ( wp_is_mobile() ) {
    ?>
    <nav class="mobile-nav">
        <ul class="mobile-list">
            <div class="close-mobile-nav undisplayed">
                <span class="first-span"></span>
                <span class="second-span"></span>
            </div>
            <li class="primary-item">
                <a href="<?php echo site_url(); ?>">Početna</a>
            </li>
            <li class="primary-item primary-item-knjige">
                <a class="clickable" href="javascript:void(0)">Knjige</a>
                <div class="primary-item-knjige-submenu primary-item-submenu undisplayed">
                    <ul class="category-list submenu-list">
                        <li>
                            <?php
                            $cat_args = array(
                                'orderby'    => 'name',
                                'order'      => 'asc',
                                'hide_empty' => true,
                                'parent' => 0,
                                'exclude' => array(15,49,52,53),
                            );
                            $product_categories = get_terms( 'product_cat', $cat_args );
                            if( ! empty( $product_categories ) ){
                                foreach ( $product_categories as $key => $category ) {
                                    echo '<a href="' . get_term_link( $category ) . '">' . $category->name . ' <span>(' . $category->count . ')</span></a>';
                                }
                            }
                            ?>
                        </li>
                    </ul>
                </div>
            </li>
            <li class="primary-item">
                <a href="<?php echo site_url(); ?>/kategorija-proizvoda/knjige-za-decu/">Knjige za decu</a>
            </li>
            <li class="primary-item">
                <a href="<?php echo site_url(); ?>/kategorija-proizvoda/najpopularnije/">Najpopularnije knjige</a>
            </li>
            <li class="primary-item">
                <a href="<?php echo site_url(); ?>/vesti/">Vesti</a>
            </li>
            <li class="primary-item"><a href="<?php echo site_url(); ?>/kontakt/">Kontakt</a></li>
            <li class="primary-item"><a href="#">011/41 43 490</a></li>
            <?php
            if ( ! is_user_logged_in() ) {
                ?>
                <li class="primary-item">
                    <a class="clickable" href="javascript:void(0)">Registruj se</a>
                    <div class="undisplayed primary-item-submenu primary-item-reg-submenu">
                        <form method="post" class="register">
                            <h3>Novi nalog</h3>
                            <?php do_action( 'woocommerce_register_form_start' ); ?>
                            <?php if ( 'no' === get_option( 'woocommerce_registration_generate_username' ) ) : ?>
                                <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                                    <label for="reg_username"><?php esc_html_e( 'Username', 'woocommerce' ); ?> <span class="required">*</span></label>
                                    <input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="username" id="reg_username" value="<?php echo ( ! empty( $_POST['username'] ) ) ? esc_attr( wp_unslash( $_POST['username'] ) ) : ''; ?>" /><?php // @codingStandardsIgnoreLine ?>
                                </p>
                            <?php endif; ?>
                            <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                                <label for="reg_email"><?php esc_html_e( 'Email', 'woocommerce' ); ?> <span class="required">*</span></label>
                                <input type="email" class="woocommerce-Input woocommerce-Input--text input-text" name="email" id="reg_email" value="<?php echo ( ! empty( $_POST['email'] ) ) ? esc_attr( wp_unslash( $_POST['email'] ) ) : ''; ?>" /><?php // @codingStandardsIgnoreLine ?>
                            </p>
                            <?php if ( 'no' === get_option( 'woocommerce_registration_generate_password' ) ) : ?>

                                <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                                    <label for="reg_password"><?php esc_html_e( 'Password', 'woocommerce' ); ?> <span class="required">*</span></label>
                                    <input type="password" class="woocommerce-Input woocommerce-Input--text input-text" name="password" id="reg_password" />
                                </p>
                            <?php endif; ?>
                            <?php do_action( 'woocommerce_register_form' ); ?>
                            <p class="woocommerce-FormRow form-row">
                                <?php wp_nonce_field( 'woocommerce-register', 'woocommerce-register-nonce' ); ?>
                                <button type="submit" class="woocommerce-Button button" name="register" value="<?php esc_attr_e( 'Register', 'woocommerce' ); ?>"> <?php esc_html_e( 'Kreiraj nalog', 'woocommerce' ); ?></button>
                            </p>
                            <?php do_action( 'woocommerce_register_form_end' ); ?>
                        </form>
                    </div>
                </li>
                <li class="primary-item">
                    <a class="clickable" href="javascript:void(0)">Prijavi se</a>
                    <div class="undisplayed primary-item-submenu primary-item-prijava-submenu">
                        <form class="woocommerce-form woocommerce-form-login login" method="post">
                            <h3>Prijavi se</h3>
                            <?php do_action( 'woocommerce_login_form_start' ); ?>
                            <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                                <label for="username"><?php esc_html_e( 'Korisničko ime ili email', 'woocommerce' ); ?> <span class="required">*</span></label>
                                <input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="username" id="username" value="<?php echo ( ! empty( $_POST['username'] ) ) ? esc_attr( wp_unslash( $_POST['username'] ) ) : ''; ?>" /><?php // @codingStandardsIgnoreLine ?>
                            </p>
                            <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                                <label for="password"><?php esc_html_e( 'Password', 'woocommerce' ); ?> <span class="required">*</span></label>
                                <input class="woocommerce-Input woocommerce-Input--text input-text" type="password" name="password" id="password" />
                            </p>
                            <?php do_action( 'woocommerce_login_form' ); ?>
                            <p class="form-row">
                                <?php wp_nonce_field( 'woocommerce-login', 'woocommerce-login-nonce' ); ?>

                                <label class="woocommerce-form__label woocommerce-form__label-for-checkbox inline">
                                    <input class="woocommerce-form__input woocommerce-form__input-checkbox" name="rememberme" type="checkbox" id="rememberme" value="forever" /> <span><?php esc_html_e( 'Zapamti me', 'woocommerce' ); ?></span>
                                </label>
                            </p>
                            <p class="woocommerce-LostPassword lost_password">
                                <a href="<?php echo esc_url( wp_lostpassword_url() ); ?>"><?php esc_html_e( 'Zaboravili ste lozinku?', 'woocommerce' ); ?></a>
                            </p>
                            <button type="submit" class="woocommerce-Button button" name="login" value="<?php esc_attr_e( 'Login', 'woocommerce' ); ?>"><?php esc_html_e( 'Uloguj se', 'woocommerce' ); ?></button>
                            <?php do_action( 'woocommerce_login_form_end' ); ?>
                        </form>
                    </div>
                </li>
                <?php
            } else {
                ?>
                <li class="primary-item primary-item-nalog">
                    <a class="clickable" href="javascript:void(0)">Moj nalog</a>
                    <div class="primary-item-submenu primary-item-nalog-submenu undisplayed">
                        <ul class="submenu-list moj-nalog-list">
                            <?php
                            $myaccount_page = get_option( 'woocommerce_myaccount_page_id' );
                            $myaccount_page_url = get_permalink( $myaccount_page );
                            $logout_url = wp_logout_url( get_permalink( $myaccount_page ) );
                            ?>
                            <li><a href="<?php echo wc_get_page_permalink( 'myaccount' ); ?>/orders">Narudžbine</a></li>
                            <li><a href="<?php echo wc_get_page_permalink( 'myaccount' ); ?>/lista-zelja/">Lista želja</a></li>
                            <li><a href="<?php echo wc_get_page_permalink( 'myaccount' ); ?>/edit-address/">Adrese</a></li>
                            <li><a href="<?php echo wc_customer_edit_account_url(); ?>">Detalji naloga</a></li>
                            <li><a href="<?php echo wp_logout_url( get_permalink( $myaccount_page ) ); ?>">Odjavi se</a></li>
                        </ul>
                    </div>
                </li>
                <?php
            }
            ?>
        </ul>
    </nav>
<?php
}
