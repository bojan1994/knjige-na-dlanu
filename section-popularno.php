<div class="container popularno-container">
<div class="naslovna-carousel-naslov">
    <a href="<?php echo home_url('/kategorija-proizvoda/najpopularnije/'); ?>"><h2>Popularno</h2></a>
    <a class="pogledaj-sve" href="<?php echo home_url('/kategorija-proizvoda/najpopularnije/'); ?>" title="<?php _e('Popularno', 'srkileee-framework'); ?>">pogledaj sve</a>
</div>
<?php $most_popular = explode(',',get_theme_mod( 'custom_most_popular' )); ?>
<div class="owl-carousel-popularno owl-carousel owl-theme owl-loaded vrteska">

<?php global $post;
$args2 = array(
    'post_type' => 'product',
    'post__in' => $most_popular,
    'posts_per_page' => '12',
    'orderby' => 'post__in',
    'no_found_rows' => true,
    'cache_results'          => false,
    'update_post_meta_cache' => false,
    'update_post_term_cache' => false,
);
$popularno = get_posts( $args2 );
foreach ( $popularno as $post ) :
  setup_postdata( $post ); ?>
  <div>
    <div class="owl-carousel-single-image">
      <a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title(); ?>">
          <?php the_post_thumbnail('medium', array('alt' => ''.get_the_title().'', 'title' => ''.get_the_title().'', 'itemprop' => 'image', 'class' => 'lazy', 'data-src' => ''.get_the_post_thumbnail_url('', 'medium' ).'', )); ?>
        </a>
      <a href="/?add-to-cart=<?php the_ID(); ?>" data-quantity="1" class="button product_type_simple add_to_cart_button ajax_add_to_cart" data-product_id="<?php the_ID(); ?>" data-product_sku="" aria-label="Add <?php the_title(); ?> to your cart" rel="nofollow" title="Dodaj u korpu">Dodaj u korpu</a>

      </div>
      <a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title(); ?>"><h3 class="naslovna-author-name"><?php the_title(); ?></h3></a>
      <?php global $woocommerce;
                    $currency = get_woocommerce_currency_symbol();
                    $price = get_post_meta( get_the_ID(), '_regular_price', true);
                    $sale = get_post_meta( get_the_ID(), '_sale_price', true);
                    ?>

                    <?php if($sale) : ?>
                    <span class="price"><del><span class="woocommerce-Price-amount amount"><?php echo $price; ?>&nbsp;<span class="woocommerce-Price-currencySymbol"><?php echo $currency; ?></span></span></del> <ins><span class="woocommerce-Price-amount amount"><?php echo $sale; ?>&nbsp;<span class="woocommerce-Price-currencySymbol"><?php echo $currency; ?></span></span></ins></span>
                    <?php elseif($price) : ?>
                    <span class="price"><span class="woocommerce-Price-amount amount"><?php echo $price; ?>&nbsp;<span class="woocommerce-Price-currencySymbol"><?php echo $currency; ?></span></span></span>
                    <?php endif; ?>
    </div>
<?php
endforeach;
wp_reset_postdata();
?>
</div>
</div>
<?php
if( ! wp_is_mobile() ) {
    ?>
    <div class="container banners-container clearfix">
        <div class="two-columns">
            <a href="https://www.knjigenadlanu.com/proizvod/komplet-knjiga-html5-css3-i-javascript/"><img class="img-responsive" src="<?php bloginfo('template_url'); ?>/assets/images/banner3.jpg"></a>
        </div>
        <div class="two-columns">
            <a href="https://www.knjigenadlanu.com/proizvod/lernelingu/"><img class="img-responsive" src="<?php bloginfo('template_url'); ?>/assets/images/banner4.jpg"></a>
        </div>
    </div>
    <?php
} else {
    ?>
    <div class="img-responsive container banners-container clearfix">
        <a href="#"><img class="img-responsive" src="<?php bloginfo('template_url'); ?>/assets/images/banner2.jpg"></a>
    </div>
    <?php
}
?>
