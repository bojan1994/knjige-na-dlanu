<?php

/* Template Name: Politika privatnosti */

get_header();

while ( have_posts() ) :
        the_post(); ?>
        <div class="politika-privatnosti-container container">
        <h1><?php the_title(); ?></h1>
        <?php the_content(); ?>
        </div>
    <?php endwhile;


get_footer();
