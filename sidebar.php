<?php

if(is_active_sidebar('sidebar')) :
    ?>
    <div class="footer-widget-area">
    <?php
    if ( is_page( 'vesti' ) || is_category( 'vesti' ) ) {
        dynamic_sidebar('sidebar');
    }
    ?>
    </div>
    <?php
endif;

?>
