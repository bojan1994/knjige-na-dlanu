<?php

/*
Template Name: Domaći autori
*/

get_header(); ?>

<h1>Autori</h1>

<?php
$terms = get_terms( array(
    'taxonomy' => 'autor',
    'hide_empty' => false,
) );

foreach( $terms as $term ) {
    $term_link = get_term_link( $term ); ?>
    <a href="<?php echo $term_link; ?>"><?php echo $term->name; ?></a>
<?php }

get_footer();
