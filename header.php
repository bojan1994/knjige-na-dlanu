<!DOCTYPE html>
<html <?php language_attributes(); ?>>
    <head>
        <meta charset="<?php bloginfo( 'charset' ); ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://fonts.googleapis.com/css?family=Merriweather:300,400,700|Montserrat:300,400,700" rel="stylesheet">
		<?php wp_head(); ?>
    	<!-- Google Tag Manager -->
    	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    	})(window,document,'script','dataLayer','GTM-TM8P6ZN');</script>
    	<!-- End Google Tag Manager -->
        <!--Start of Zendesk Chat Script-->
        <script type="text/javascript">
        window.$zopim||(function(d,s){var z=$zopim=function(c){
        z._.push(c)},$=z.s=
        d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
        _.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute('charset','utf-8');
        $.src='https://v2.zopim.com/?5u2Dm4Cq065Vn2mU8gnn8ypeyj4BEkUM';z.t=+new Date;$.
        type='text/javascript';e.parentNode.insertBefore($,e)})(document,'script');
        </script>
        <!--End of Zendesk Chat Script-->
    </head>
    <body <?php body_class(); ?>>
    	<!-- Google Tag Manager (noscript) -->
    	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TM8P6ZN"
    	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    	<!-- End Google Tag Manager (noscript) -->
        <header class="site-header clearfix">

                <div class="header-paragraph clearfix">
                    <div class="container">
                        <?php
                        if ( ! wp_is_mobile() ) {
                            ?>
                            <div class="secondary-navigation">
                                <div class="menu-sekundarna-navigacija-container">
                                    <ul id="menu-sekundarna-navigacija" class="menu">
                                        <li id="menu-item-39" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-39">
                                            <a href="<?php echo site_url(); ?>/kontakt/"><i class="icon-email"></i> Kontakt</a>
                                        </li>
                                        <li id="menu-item-40" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-40">
                                            <a href="#"><i class="icon-phone"></i> 011/41 43 490</a>
                                        </li>
                                        <?php
                                        if ( ! is_user_logged_in() ) {
                                            ?>
                                            <li class="registruj-se-list-item open-sub-menu-box menu-item menu-item-type-custom menu-item-object-custom menu-item-40">
                                                <a class="open-sub-menu-box-link" href="#">Registruj se</a>
                                                <div class="register-sub sub-menu-box">
                                                    <form method="post" class="register">
                                            			<?php do_action( 'woocommerce_register_form_start' ); ?>
                                            			<?php if ( 'no' === get_option( 'woocommerce_registration_generate_username' ) ) : ?>
                                            				<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                                            					<label for="reg_username"><?php esc_html_e( 'Username', 'woocommerce' ); ?> <span class="required">*</span></label>
                                            					<input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="username" id="reg_username" value="<?php echo ( ! empty( $_POST['username'] ) ) ? esc_attr( wp_unslash( $_POST['username'] ) ) : ''; ?>" /><?php // @codingStandardsIgnoreLine ?>
                                            				</p>
                                            			<?php endif; ?>
                                            			<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                                            				<label for="reg_email"><?php esc_html_e( 'Email', 'woocommerce' ); ?> <span class="required">*</span></label>
                                            				<input type="email" class="woocommerce-Input woocommerce-Input--text input-text" name="email" id="reg_email" value="<?php echo ( ! empty( $_POST['email'] ) ) ? esc_attr( wp_unslash( $_POST['email'] ) ) : ''; ?>" /><?php // @codingStandardsIgnoreLine ?>
                                            			</p>
                                            			<?php if ( 'no' === get_option( 'woocommerce_registration_generate_password' ) ) : ?>

                                            				<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                                            					<label for="reg_password"><?php esc_html_e( 'Password', 'woocommerce' ); ?> <span class="required">*</span></label>
                                            					<input type="password" class="woocommerce-Input woocommerce-Input--text input-text" name="password" id="reg_password" />
                                            				</p>
                                            			<?php endif; ?>
                                            			<?php do_action( 'woocommerce_register_form' ); ?>
                                            			<p class="woocommerce-FormRow form-row">
                                            				<?php wp_nonce_field( 'woocommerce-register', 'woocommerce-register-nonce' ); ?>
                                            				<button type="submit" class="woocommerce-Button button" name="register" value="<?php esc_attr_e( 'Register', 'woocommerce' ); ?>"> <?php esc_html_e( 'Kreiraj nalog', 'woocommerce' ); ?></button>
                                            			</p>
                                            			<?php do_action( 'woocommerce_register_form_end' ); ?>
                                            		</form>
                                                </div>
                                            </li>
                                            <li  class="prijavi-se-list-item open-sub-menu-box menu-item menu-item-type-custom menu-item-object-custom menu-item-40">
                                                <a class="open-sub-menu-box-link" href="#">Prijavi se</a>
                                                <div class="prijavi-se-sub sub-menu-box">
                                                    <form class="woocommerce-form woocommerce-form-login login" method="post">
                                            			<?php do_action( 'woocommerce_login_form_start' ); ?>
                                            			<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                                            				<label for="username"><?php esc_html_e( 'Korisničko ime ili email', 'woocommerce' ); ?> <span class="required">*</span></label>
                                            				<input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="username" id="username" value="<?php echo ( ! empty( $_POST['username'] ) ) ? esc_attr( wp_unslash( $_POST['username'] ) ) : ''; ?>" /><?php // @codingStandardsIgnoreLine ?>
                                            			</p>
                                            			<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                                            				<label for="password"><?php esc_html_e( 'Password', 'woocommerce' ); ?> <span class="required">*</span></label>
                                            				<input class="woocommerce-Input woocommerce-Input--text input-text" type="password" name="password" id="password" />
                                            			</p>
                                            			<?php do_action( 'woocommerce_login_form' ); ?>
                                            			<p class="form-row">
                                            				<?php wp_nonce_field( 'woocommerce-login', 'woocommerce-login-nonce' ); ?>

                                            				<label class="woocommerce-form__label woocommerce-form__label-for-checkbox inline">
                                            					<input class="woocommerce-form__input woocommerce-form__input-checkbox" name="rememberme" type="checkbox" id="rememberme" value="forever" /> <span><?php esc_html_e( 'Zapamti me', 'woocommerce' ); ?></span>
                                            				</label>
                                            			</p>
                                            			<p class="woocommerce-LostPassword lost_password">
                                            				<a href="<?php echo esc_url( wp_lostpassword_url() ); ?>"><?php esc_html_e( 'Zaboravili ste lozinku?', 'woocommerce' ); ?></a>
                                            			</p>
                                                        <button type="submit" class="woocommerce-Button button" name="login" value="<?php esc_attr_e( 'Login', 'woocommerce' ); ?>"><?php esc_html_e( 'Uloguj se', 'woocommerce' ); ?></button>
                                            			<?php do_action( 'woocommerce_login_form_end' ); ?>
                                            		</form>
                                                </div>
                                            </li>
                                            <?php
                                        } else {
                                            ?>
                                            <li id="menu-item-92" class="moj-nalog-list-item open-sub-menu-box menu-item menu-item-type-post_type menu-item-object-page menu-item-92">
                                                <a class="open-sub-menu-box-link" href="#">Moj nalog</a>
                                                <div class="moj-nalog-sub sub-menu-box">
                                                    <ul>
                                                        <?php
                                                        $myaccount_page = get_option( 'woocommerce_myaccount_page_id' );
                                                        $myaccount_page_url = get_permalink( $myaccount_page );
                                                        $logout_url = wp_logout_url( get_permalink( $myaccount_page ) );
                                                        ?>
                                                        <li><a href="<?php echo wc_get_page_permalink( 'myaccount' ); ?>/orders">Narudžbine</a></li>
                                                        <li><a href="<?php echo wc_get_page_permalink( 'myaccount' ); ?>/lista-zelja/">Lista želja</a></li>
                                                        <li><a href="<?php echo wc_get_page_permalink( 'myaccount' ); ?>/edit-address/">Adrese</a></li>
                                                        <li><a href="<?php echo wc_customer_edit_account_url(); ?>">Detalji naloga</a></li>
                                                        <li><a href="<?php echo wp_logout_url( get_permalink( $myaccount_page ) ); ?>">Odjavi se</a></li>
                                                    </ul>
                                                </div>
                                            </li>
                                            <?php
                                        }
                                        ?>
                                        <li id="menu-item-97" class="menu-item cart-list-item open-sub-menu-box menu-item-type-post_type menu-item-object-page menu-item-97">
                                            <a class="open-sub-menu-box-link" href="#"><div id="cart-fragment"><i class="icon-cart"></i> <div class="cart-counter"><span><?php echo WC()->cart->get_cart_contents_count() ?></span></div></div></a>
                                            <div class="mini-cart sub-menu-box">
                                              <div id="cart-fragment-sec">
                                                <?php
                                                foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) { ?>
                                                    <div class="mini-narudzbina">
                                                        <?php
                                        				$_product   = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
                                        				$product_id = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );
                                                        ?>
                                                        <div class="mini-narudzbina-image">
                                                            <?php
                                                            $image = wp_get_attachment_image_src( get_post_thumbnail_id( $_product->get_id() ), 'full' );
                                                            ?>
                                                            <a href="<?php the_permalink( $product_id ); ?>"><img src="<?php echo $image[0]; ?>" alt="<?php echo $_product->get_name(); ?>" title="<?php echo $_product->get_name(); ?>"></a>
                                                        </div>
                                                        <?php
                                                        $terms = wp_get_post_terms( $_product->get_id(), 'autor' ); ?>
                                                        <div class="mini-narudzbina-author">
                                                            <?php
                                                            /* if( $terms ) {
                                                                 ?>
                                                                 <!-- <p class="mini-cart-name"><?php echo $terms[0]->name; ?></p> -->
                                                                 <?php
                                                            } */
                                                            ?>
                                                            <a href="<?php the_permalink( $product_id ); ?>"><p class="mini-cart-book"><?php echo $_product->get_name(); ?></p></a>
                                                        </div>
                                                        <?php
                                                        $targeted_id = $product_id;
                                                        foreach ( WC()->cart->get_cart() as $cart_item ) {
                                                            if($cart_item['product_id'] == $targeted_id ) {
                                                                $qty =  $cart_item['quantity'];
                                                                break;
                                                            }
                                                        }
                                                        if( ! empty( $qty ) ) {
                                                            ?>
                                                            <div class="mini-narudzbina-price">
                                                                <p><?php echo $qty; ?> x <?php echo WC()->cart->get_product_price( $_product ); ?></p>
                                                            </div>
                                                            <?php
                                                        }
                                                        ?>
                                                    </div>
                                    			<?php }
                                                global $woocommerce;
                                                ?>
                                                <div class="mini-cart-ukupno">
                                                    <p class="mini-cart-ukupno-cifra">Ukupno: <span><?php echo $woocommerce->cart->get_cart_total(); ?></span></p>
                                                    <p class="mini-cart-ukupno-napomena">(Troškovi isporuke nisu uračunati)</p>
                                                </div>
                                                <div class="mini-cart-buttons">
                                                    <a class="pregled-korpe" href="<?php echo wc_get_cart_url(); ?>">Pregled korpe</a>
                                                    <a class="placanje" href="<?php echo wc_get_checkout_url(); ?>">Plaćanje</a>
                                                </div>
                                            </div>
                                          </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                </div>

                <div class="main-navigation clearfix">
                    <?php if ( wp_is_mobile() ) : ?>
                        <div class="header-mobile-top">
                            <div class="container header-mobile-container-top">
                                <div class="site-logo">
                                    <?php
                                    $custom_logo = get_theme_mod( 'custom_logo', array( 'class' => 'img-responsive' ) );
                                    $image = wp_get_attachment_image_src( $custom_logo, 'full' );
                                    if ( is_front_page() ) {
                                        ?>
                                        <h1>
                                            <span class="sr-only"><?php echo bloginfo( 'name' ); ?></span>
                                        <?php
                                    }
                                    ?>
                                    <a href="<?php echo home_url(); ?>">
                                        <img src="<?php echo $image[0]; ?>" alt="<?php echo bloginfo( 'name' ); ?>" title="<?php echo bloginfo( 'name' ); ?>">
                                    </a><?php
                                    if ( is_front_page() ) {
                                        ?>
                                        </h1>
                                        <?php
                                    } ?>
                                </div>
                                <div class="header-mobile-icons">
                                    <div class="mobile-search">
                                        <a href="#"><i class="icon-search"></i></a>
                                    </div>
                                    <div class="mobile-cart">
                                        <a class="clickable" href="javascript:void(0)"><div id="cart-fragment"><i class="icon-cart"></i> <div class="cart-counter"><span><?php echo WC()->cart->get_cart_contents_count() ?></span></div></div></a>
                                        <div class="primary-item-submenu primary-item-cart-submenu undisplayed">
                                            <div class="cart-result">
                                                <div id="cart-fragment-sec">
                                                    <?php
                                                    foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) { ?>
                                                        <div class="mini-narudzbina">
                                                            <?php
                                                            $_product   = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
                                                            $product_id = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );
                                                            ?>
                                                            <div class="mini-narudzbina-image">
                                                                <?php the_post_thumbnail( 'news-image', array('alt' => get_the_title().'', 'title' => get_the_title().'' ) ); ?>
                                                            </div>
                                                            <?php
                                                            $terms = wp_get_post_terms( $_product->get_id(), 'autor' ); ?>
                                                            <div class="">
                                                                <p class=""><?php echo $_product->get_name(); ?></p>
                                                            </div>
                                                            <?php
                                                            $targeted_id = $product_id;
                                                            foreach ( WC()->cart->get_cart() as $cart_item ) {
                                                                if($cart_item['product_id'] == $targeted_id ){
                                                                    $qty =  $cart_item['quantity'];
                                                                    break;
                                                                }
                                                            }
                                                            if( ! empty( $qty ) ) {
                                                                ?>
                                                                <div class="">
                                                                    <p><?php echo $qty; ?> x <?php echo WC()->cart->get_product_price( $_product ); ?></p>
                                                                </div>
                                                                <?php
                                                            } ?>
                                                        </div>
                                                    <?php }
                                                    $total = WC()->cart->cart_contents_total;
                                                    ?>
                                                    <div class="troskovi-cart">
                                                        <p class="">Ukupno: <span><?php echo $total; ?></span></p>
                                                        <p class="">(Troškovi isporuke nisu uračunati)</p>
                                                    </div>
                                                    <div class="cart-buttons">
                                                        <a class="pregled-korpe" href=<?php echo wc_get_cart_url(); ?>>Pregled korpe</a>
                                                        <a class="troskovi" href=<?php echo wc_get_checkout_url(); ?>>Plaćanje</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="hamburger">
                                    <a id="menu-icon" class="hamburger">
                                        <span class="hamburger__top-bun"></span>
                                        <span class="hamburger__middle-bun"></span>
                                        <span class="hamburger__bottom-bun"></span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <div class="container header-container">
                        <?php
                        if( ! wp_is_mobile() ) {
                            ?>
                            <div class="site-logo">
                                <?php
                                $custom_logo = get_theme_mod( 'custom_logo', array( 'class' => 'img-responsive' ) );
                                $image = wp_get_attachment_image_src( $custom_logo, 'full' );
                                if ( is_front_page() ) {
                                    ?>
                                    <h1>
                                        <span class="sr-only"><?php echo bloginfo( 'name' ); ?></span>
                                    <?php
                                }
                                ?>
                                <a href="<?php echo home_url(); ?>">
                                    <img src="<?php echo $image[0]; ?>" alt="<?php echo bloginfo( 'name' ); ?>" title="<?php echo bloginfo( 'name' ); ?>">
                                </a><?php
                                if ( is_front_page() ) {
                                    ?>
                                    </h1>
                                    <?php
                                } ?>
                            </div>
                            <?php
                        }
                        ?>

                        <?php
                        if ( ! wp_is_mobile() ) {
                            ?>
                            <nav class="site-nav clearfix">
                                <div class="menu-primarna-navigacija-container">
                                    <ul id="menu-primarna-navigacija" class="menu">
                                        <li>
                                            <a href="<?php echo site_url(); ?>">Početna</a>
                                        </li>
                                        <li id="menu-item-93" class="menu-item main-menu-item-has-children menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item current_page_item menu-item-93">
                                            <a href="#">Knjige</a>
                                            <ul class="nav-sub-menu">
                                                <div class="container nav-sub-menu-container">
                                                    <div class="one-column">
                                                        <h2>Knjige</h2>
                                                    </div>
                                                    <li class="nav-sub-menu-lists">
                                                        <?php
                                                        $cat_args = array(
                                                            'orderby'    => 'name',
                                                            'order'      => 'asc',
                                                            'hide_empty' => true,
                                                            'parent' => 0,
                                                            'exclude' => array( 5162 ),
                                                        );
                                                        $product_categories = get_terms( 'product_cat', $cat_args );
                                                        $limit = count( $product_categories );
                                                        $cat_number = $limit / 2;
                                                        $round_cat_number = intval( round( $cat_number ) );
                                                        $counter = 0;
                                                        if( ! empty( $product_categories ) ){
                                                            foreach ( $product_categories as $key => $category ) {
                                                                $counter++;
                                                                if ( $counter === $round_cat_number + 1 ) {
                                                                    $limit -= $counter;
                                                                    $counter = 1;
                                                                }
                                                                if ( $counter == 1 ) {
                                                                    echo '<div class="nav-sub-menu-list">';
                                                                }
                                                                echo '<a href="' . get_term_link( $category ) . '">' . $category->name . ' <span>(' . $category->count . ')</span></a>';
                                                                if ( $counter % $round_cat_number == 0 || $limit == $counter - 1 ) {
                                                                    echo '</div>';
                                                                }
                                                            }
                                                        }
                                                        ?>
                                                        <div class="nav-sub-menu-list">
                                                            <?php
                                                            $menu_books = explode( ',', get_theme_mod( 'custom_menu_book' ) );
                                                            $args_menu_books = array(
                                                                'post_type' => 'product',
                                                                'posts_per_page' => 2,
                                                                'post__in' => $menu_books,
                                                                'orderby' => 'post__in',
                                                            );
                                                            $query_menu_books = new WP_Query( $args_menu_books );
                                                            if( $query_menu_books->have_posts() ) :
                                                                while( $query_menu_books->have_posts() ) :
                                                                    $query_menu_books->the_post();
                                                                    ?>
                                                                    <div class="nav-sub-menu-list-book">
                                                                          <?php
                                                                          global $product;
                                                                          ?>
                                                                          <div class="nav-sub-menu-list-book-relative">
                                                                            <a href="<?php the_permalink(); ?>">
                                                                                <?php
                                                                                the_post_thumbnail( 'carousel-image', array( 'class' => 'img-responsive', 'alt' => get_the_title(), 'title' => get_the_title() ) );
                                                                                ?>
                                                                                <a href="/?add-to-cart=<?php the_ID(); ?>" data-quantity="1" class="button product_type_simple add_to_cart_button ajax_add_to_cart" data-product_id="<?php the_ID(); ?>" data-product_sku="" aria-label="Add <?php the_title(); ?> to your cart" rel="nofollow" title="Dodaj u korpu">Dodaj u korpu</a>
                                                                            </a>
                                                                          </div>
                                                                          <a href="<?php the_permalink(); ?>">
                                                                              <?php
                                                                              echo $product->get_name();
                                                                              ?>
                                                                          </a>
                                                                          <?php
                                                                          echo $product->get_price_html();
                                                                          ?>
                                                                    </div>
                                                                    <?php
                                                                endwhile;
                                                                wp_reset_postdata();
                                                            endif;
                                                            ?>
                                                        </div>
                                                    </li>
                                                </div>
                                            </ul>
                                        </li>
                                        <li id="menu-item-31" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-31">
                                            <a href="<?php echo site_url(); ?>/kategorija-proizvoda/knjige-za-decu/">Knjige za decu</a>
                                        </li>
                                        <li id="menu-item-32" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-32">
                                            <a href="<?php echo site_url(); ?>/kategorija-proizvoda/najpopularnije/">Najpopularnije knjige</a>
                                        </li>
                                        <li id="menu-item-33" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-33">
                                            <a href="<?php echo site_url(); ?>/vesti/">Vesti</a>
                                        </li>
                                    </ul>
                                </div>
                            </nav>
                            <?php
                        }
                        ?>

                        <?php get_template_part('content','mobile-nav'); ?>

                        <div<?php echo wp_is_mobile() ? ' class="search-wrapper undisplayed"' : ''; ?>>
                            <?php dynamic_sidebar( 'search_bar' ); ?>
                        </div>

                    </div>
                </div>

                <?php
                if ( is_front_page() ) {
                    ?>
                    <div class="flexslider">
                        <ul class="slides">
                            <?php
                            $args = array(
                                'post_type' => 'slider',
                            );
                            $query = new WP_Query( $args );
                            if ( $query->have_posts() ) :
                                while( $query->have_posts() ) :
                                    $query->the_post(); ?>
                                            <li>
                                                <a href="<?php echo get_post_meta( $post->ID,'_url_value_key',true ); ?>"><?php the_post_thumbnail( 'full', array( 'class' => 'img-responsive', 'alt' => get_the_title(), 'title' => get_the_title() ) ); ?></a>
                                            </li>
                                <?php endwhile;
                            else :
                                _e( 'Sorry, no content found', 'Knjige na dlanu' );
                            endif;
                            ?>
                        </ul>
                    </div>
                    <?php
                }
                ?>

        </header>

        <?php
        if( wp_is_mobile() ) {
            ?>
            <div class="img-responsive container banners-container clearfix">
                <a href="https://www.knjigenadlanu.com/proizvod/pingulingo/"><img class="img-responsive" src="<?php bloginfo('template_url'); ?>/assets/images/banner6.jpg"></a>
            </div>
            <?php
        }
        ?>
