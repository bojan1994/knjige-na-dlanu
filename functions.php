<?php

require_once __DIR__ . '/vendor/autoload.php';

function disable_wp_emojicons() {

  // all actions related to emojis
  remove_action( 'admin_print_styles', 'print_emoji_styles' );
  remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
  remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
  remove_action( 'wp_print_styles', 'print_emoji_styles' );
  remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
  remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
  remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );

  // filter to remove TinyMCE emojis
  add_filter( 'tiny_mce_plugins', 'disable_emojicons_tinymce' );
}
add_action( 'init', 'disable_wp_emojicons' );

function disable_emojicons_tinymce( $plugins ) {
  if ( is_array( $plugins ) ) {
    return array_diff( $plugins, array( 'wpemoji' ) );
  } else {
    return array();
  }
}

add_filter( 'emoji_svg_url', '__return_false' );

/* Register scripts */
function register_scripts() {
    wp_enqueue_style( 'icomoon', get_template_directory_uri() . '/assets/css/icomoon.style.css' );
    wp_enqueue_style( 'style', get_template_directory_uri() . '/style.min.css' );
    wp_enqueue_style( 'owl.carousel', get_template_directory_uri() . '/assets/css/owl.carousel.min.css' );
    wp_enqueue_style( 'owl.theme.default', get_template_directory_uri() . '/assets/css/owl.theme.default.min.css' );
    wp_enqueue_style( 'owl.theme.green', get_template_directory_uri() . '/assets/css/owl.theme.green.min.css' );
    wp_enqueue_script( 'owl.carousel', get_template_directory_uri() . '/assets/scripts/owl.carousel.min.js', array( 'jquery' ), false, true );
    wp_enqueue_script( 'flexslider', get_template_directory_uri() . '/assets/scripts/jquery-flexslider-min.js', array( 'jquery' ), false, true );
    wp_enqueue_script( 'matchHeight', get_template_directory_uri() . '/assets/scripts/jquery.matchHeight-min.js', array( 'jquery' ), false, true );
    wp_enqueue_script( 'main', get_template_directory_uri() . '/main.min.js', array( 'flexslider', 'owl.carousel', 'matchHeight' ), false, true );
    wp_enqueue_script( 'main', get_template_directory_uri() . '/assets/scripts/srkileee-site-functions.js', array( ), false, true );
}
add_action( 'wp_enqueue_scripts','register_scripts' );

/* Theme Setup */
if( ! function_exists( 'theme_setup' ) ) {
    function theme_setup() {
        $array = array(
            'height'      => 40,
            'width'       => 235,
            'flex-height' => true,
            'flex-width'  => true,
            'header-text' => array( 'site-title', 'site-description' ),
        );
        add_theme_support( 'custom-logo', $array );
        add_image_size( 'news-image', 320, 210, true );
        add_image_size( 'carousel-image', 200, 300, true );
        add_theme_support( 'title-tag' );
        add_theme_support( 'woocommerce' );
    }
    add_action( 'after_setup_theme','theme_setup' );
}

/* Custom footer */
if( ! function_exists( 'theme_customizer' ) ) {
    function theme_customizer( $wp_customize ) {
        $wp_customize->add_setting( 'custom_footer_text', array(
            'default' => '',
            'transport' => 'refresh',
        ) );
        $wp_customize->add_section( 'footer_text',array(
            'title' => __( 'Custom footer text', 'Knige na dlanu' ),
            'priority' => 160,
        ) );
        $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'custom_footer', array(
            'label' => __( 'Footer Text', 'Knige na dlanu' ),
            'section' => 'footer_text',
            'settings' => 'custom_footer_text',
        ) ) );
    }
    add_action( 'customize_register', 'theme_customizer' );
}

/* Most popular */
if( ! function_exists( 'most_popular_customizer' ) ) {
    function most_popular_customizer( $wp_customize ) {
        $wp_customize->add_setting( 'custom_most_popular', array(
            'default' => '',
            'transport' => 'refresh',
        ) );
        $wp_customize->add_section( 'most_popular',array(
            'title' => __( 'Najpopularnije', 'Knige na dlanu' ),
            'priority' => 160,
        ) );
        $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'custom_popular', array(
            'label' => __( 'Najpopularnije', 'Knige na dlanu' ),
            'section' => 'most_popular',
            'settings' => 'custom_most_popular',
        ) ) );
    }
    add_action( 'customize_register', 'most_popular_customizer' );
}

/* Menu book */
if( ! function_exists( 'menu_book_customizer' ) ) {
    function menu_book_customizer( $wp_customize ) {
        $wp_customize->add_setting( 'custom_menu_book', array(
            'default' => '',
            'transport' => 'refresh',
        ) );
        $wp_customize->add_section( 'menu_book',array(
            'title' => __( 'Meni', 'Knige na dlanu' ),
            'priority' => 160,
        ) );
        $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'book', array(
            'label' => __( 'Meni', 'Knige na dlanu' ),
            'section' => 'menu_book',
            'settings' => 'custom_menu_book',
        ) ) );
    }
    add_action( 'customize_register', 'menu_book_customizer' );
}

/* Our recomendations */
if( ! function_exists( 'our_recomendations_customizer' ) ) {
    function our_recomendations_customizer( $wp_customize ) {
        $wp_customize->add_setting( 'custom_our_recomendations', array(
            'default' => '',
            'transport' => 'refresh',
        ) );
        $wp_customize->add_section( 'our_recomendations',array(
            'title' => __( 'Preporučujemo', 'Knige na dlanu' ),
            'priority' => 160,
        ) );
        $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'custom_recomendations', array(
            'label' => __( 'Preporučujemo', 'Knige na dlanu' ),
            'section' => 'our_recomendations',
            'settings' => 'custom_our_recomendations',
        ) ) );
    }
    add_action( 'customize_register', 'our_recomendations_customizer' );
}

/* Most recent on action */
if( ! function_exists( 'most_recent_customizer' ) ) {
    function most_recent_customizer( $wp_customize ) {
        $wp_customize->add_setting( 'custom_most_recent', array(
            'default' => '',
            'transport' => 'refresh',
        ) );
        $wp_customize->add_section( 'most_recent',array(
            'title' => __( 'Najnovije', 'Knige na dlanu' ),
            'priority' => 160,
        ) );
        $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'custom_recent', array(
            'label' => __( 'Najnovije', 'Knige na dlanu' ),
            'section' => 'most_recent',
            'settings' => 'custom_most_recent',
        ) ) );
    }
    add_action( 'customize_register', 'most_recent_customizer' );
}

/* Top books */
if( ! function_exists( 'top_books_customizer' ) ) {
    function top_books_customizer( $wp_customize ) {
        $wp_customize->add_setting( 'custom_top_books', array(
            'default' => '',
            'transport' => 'refresh',
        ) );
        $wp_customize->add_section( 'top_books',array(
            'title' => __( 'Top knjige', 'Knige na dlanu' ),
            'priority' => 160,
        ) );
        $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'custom_top', array(
            'label' => __( 'Top knjige', 'Knige na dlanu' ),
            'section' => 'top_books',
            'settings' => 'custom_top_books',
        ) ) );
    }
    add_action( 'customize_register', 'top_books_customizer' );
}

/* Shipping cost */
if( ! function_exists( 'shipping_cost_customizer' ) ) {
    function shipping_cost_customizer( $wp_customize ) {
        $wp_customize->add_setting( 'custom_shipping_cost', array(
            'default' => '',
            'transport' => 'refresh',
        ) );
        $wp_customize->add_section( 'shipping_cost',array(
            'title' => __( 'Informacije o dostavi do 0.5 kg', 'Knige na dlanu' ),
            'priority' => 160,
        ) );
        $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'custom_shipping', array(
            'label' => __( 'Informacije o dostavi do 0.5 kg', 'Knige na dlanu' ),
            'section' => 'shipping_cost',
            'settings' => 'custom_shipping_cost',
        ) ) );
    }
    add_action( 'customize_register', 'shipping_cost_customizer' );
}

/* Shipping cost 2 */
if( ! function_exists( 'shipping_cost_customizer2' ) ) {
    function shipping_cost_customizer2( $wp_customize ) {
        $wp_customize->add_setting( 'custom_shipping_cost2', array(
            'default' => '',
            'transport' => 'refresh',
        ) );
        $wp_customize->add_section( 'shipping_cost2',array(
            'title' => __( 'Informacije o dostavi od 0.5 kg do 2kg', 'Knige na dlanu' ),
            'priority' => 160,
        ) );
        $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'custom_shipping2', array(
            'label' => __( 'Informacije o dostavi od 0.5 kg do 2kg', 'Knige na dlanu' ),
            'section' => 'shipping_cost2',
            'settings' => 'custom_shipping_cost2',
        ) ) );
    }
    add_action( 'customize_register', 'shipping_cost_customizer2' );
}

/* Shipping cost 3 */
if( ! function_exists( 'shipping_cost_customizer3' ) ) {
    function shipping_cost_customizer3( $wp_customize ) {
        $wp_customize->add_setting( 'custom_shipping_cost3', array(
            'default' => '',
            'transport' => 'refresh',
        ) );
        $wp_customize->add_section( 'shipping_cost3',array(
            'title' => __( 'Informacije o dostavi od 2 kg do 3kg', 'Knige na dlanu' ),
            'priority' => 160,
        ) );
        $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'custom_shipping3', array(
            'label' => __( 'Informacije o dostavi od 2 kg do 3kg', 'Knige na dlanu' ),
            'section' => 'custom_shipping3',
            'settings' => 'custom_shipping_cost3',
        ) ) );
    }
    add_action( 'customize_register', 'shipping_cost_customizer3' );
}

/* Shipping cost 4 */
if( ! function_exists( 'shipping_cost_customizer4' ) ) {
    function shipping_cost_customizer4( $wp_customize ) {
        $wp_customize->add_setting( 'custom_shipping_cost4', array(
            'default' => '',
            'transport' => 'refresh',
        ) );
        $wp_customize->add_section( 'shipping_cost4',array(
            'title' => __( 'Informacije o dostavi od 3 kg do 4kg', 'Knige na dlanu' ),
            'priority' => 160,
        ) );
        $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'custom_shipping4', array(
            'label' => __( 'Informacije o dostavi od 3 kg do 4kg', 'Knige na dlanu' ),
            'section' => 'custom_shipping4',
            'settings' => 'custom_shipping_cost4',
        ) ) );
    }
    add_action( 'customize_register', 'shipping_cost_customizer4' );
}

/* Shipping cost 5 */
if( ! function_exists( 'shipping_cost_customizer5' ) ) {
    function shipping_cost_customizer5( $wp_customize ) {
        $wp_customize->add_setting( 'custom_shipping_cost5', array(
            'default' => '',
            'transport' => 'refresh',
        ) );
        $wp_customize->add_section( 'shipping_cost5',array(
            'title' => __( 'Informacije o dostavi od 4 kg do 5kg', 'Knige na dlanu' ),
            'priority' => 160,
        ) );
        $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'custom_shipping5', array(
            'label' => __( 'Informacije o dostavi od 4 kg do 5kg', 'Knige na dlanu' ),
            'section' => 'custom_shipping5',
            'settings' => 'custom_shipping_cost5',
        ) ) );
    }
    add_action( 'customize_register', 'shipping_cost_customizer5' );
}

/* Link Meta Box */
if( ! function_exists( 'link_meta_box' ) ) {
    function link_meta_box() {
        add_meta_box(
            'link_box',
            'Link',
            'link_meta_box_callback',
            'slider'
        );
    }
    add_action( 'add_meta_boxes','link_meta_box' );
}
if( ! function_exists( 'link_meta_box_callback' ) ) {
    function link_meta_box_callback( $post ) {
        wp_nonce_field( 'link_meta_box_save_data','link_meta_box_nonce' );
        $url = get_post_meta( $post->ID,'_url_value_key',true );
        echo '<label for="link_url">Url: </label>';
        echo '<input type="text" id="link_url" name="link_url" value="' . esc_attr( $url ) . '" size="25" />';
    }
}
if( ! function_exists( 'link_meta_box_save_data' ) ) {
    function link_meta_box_save_data( $post_id ) {
        if( ! isset( $_POST['link_meta_box_nonce'] ) ) {
            return;
        }
        if( ! wp_verify_nonce( $_POST['link_meta_box_nonce'],'link_meta_box_save_data' ) ) {
            return;
        }
        if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
            return;
        }
        if( ! current_user_can( 'edit_post',$post_id ) ) {
            return;
        }
        if( ! isset( $_POST['link_url'] ) ) {
            return;
        }
        $url1 = sanitize_text_field( $_POST['link_url'] );
        update_post_meta( $post_id,'_url_value_key',$url1 );
    }
    add_action( 'save_post','link_meta_box_save_data' );
}

/* Register navigations menus */
register_nav_menus( array(
    'primary' => __( 'Primary Navigation', 'Knige na dlanu' ),
    'secondary' => __( 'Secondary Navigation', 'Knjige na dlanu' ),
    'footer' => __( 'Footer Navigation', 'Knjige na dlanu' ),
    'footer2' => __( 'Footer Navigation 2', 'Knjige na dlanu' ),
) );

// Register widgets
function addWidget(){
    register_sidebar(array(
        'name'=>'Footer Area 1',
        'id'=>'footer1'
    ));
    register_sidebar(array(
        'name'=>'Footer Area 2',
        'id'=>'footer2'
    ));
    register_sidebar(array(
        'name'=>'Footer Area 3',
        'id'=>'footer3'
    ));
    register_sidebar(array(
        'name'=>'Footer Area 4',
        'id'=>'footer4'
    ));
    register_sidebar(array(
        'name'=>'Footer Area 5',
        'id'=>'footer5'
    ));
    register_sidebar(array(
        'name'=>'Search bar',
        'id'=>'search_bar'
    ));
    register_sidebar(array(
        'name'=>'Sidebar',
        'id'=>'sidebar'
    ));
    register_sidebar(array(
        'name'=>'Vec ste gledali',
        'id'=>'sidebar2',
    ));
    register_sidebar(array(
        'name'=>'ChipCard footer',
        'id'=>'footer6',
    ));
    register_sidebar(array(
        'name'=>'Footer navigation',
        'id'=>'footer7',
    ));
}
add_action('widgets_init','addWidget');

/* Slider post type */
if( ! function_exists( 'slider_post_type' ) ) {
    function slider_post_type() {
        $labels = array(
            'name' => __( 'Slider','Knjige na dlanu' ),
            'singular_name' => __( 'Slider', 'Knjige na dlanu' ),
        );
        $supports = array(
            'title',
            'editor',
            'thumbnail',
            'post-formats',
        );
        $args = array(
            'labels' => $labels,
            'supports' => $supports,
            'public' => true,
            'capability_type' => 'post',
            'show_ui' => true,
            'has_archive' => false,
            'rewrite' => array(
                'slug' => 'slider',
            ),
        );
        register_post_type( 'slider',$args );
    }
    add_action( 'init','slider_post_type' );
}

function add_excerpt_for_news_post_type() {
    add_post_type_support( 'news', 'excerpt' );
}
add_action( 'init', 'add_excerpt_for_news_post_type' );

// function create_author_taxonomies() {
// 	$labels = array(
// 		'name'              => _x( 'Author', 'taxonomy general name', 'textdomain' ),
// 		'singular_name'     => _x( 'Author', 'taxonomy singular name', 'textdomain' ),
// 		'search_items'      => __( 'Search Authors', 'textdomain' ),
// 		'all_items'         => __( 'All Authors', 'textdomain' ),
// 		'parent_item'       => __( 'Parent Author', 'textdomain' ),
// 		'parent_item_colon' => __( 'Parent Author:', 'textdomain' ),
// 		'edit_item'         => __( 'Edit Author', 'textdomain' ),
// 		'update_item'       => __( 'Update Author', 'textdomain' ),
// 		'add_new_item'      => __( 'Add New Author', 'textdomain' ),
// 		'new_item_name'     => __( 'New Author Name', 'textdomain' ),
// 		'menu_name'         => __( 'Author', 'textdomain' ),
// 	);
// 	$args = array(
// 		'hierarchical'      => true,
// 		'labels'            => $labels,
// 		'show_ui'           => true,
// 		'show_admin_column' => true,
// 		'query_var'         => true,
// 		'rewrite'           => array( 'slug' => 'autor' ),
// 	);
// 	register_taxonomy( 'autor', array( 'product' ), $args );
//
// }
// add_action( 'init', 'create_author_taxonomies');

// Register and load the widget
function wpb_load_widget() {
    register_widget( 'wpb_widget' );
}
add_action( 'widgets_init', 'wpb_load_widget' );

// Creating the widget
class wpb_widget extends WP_Widget {

    function __construct() {
    parent::__construct(

    // Base ID of your widget
    'wpb_widget',

    // Widget name will appear in UI
    __('Top knjige', 'wpb_widget_domain'),

    // Widget description
    array( 'description' => __( 'Sample widget based on WPBeginner Tutorial', 'wpb_widget_domain' ), )
    );
}

// Creating widget front-end

public function widget( $args, $instance ) {
$title = apply_filters( 'widget_title', $instance['title'] );

// before and after widget arguments are defined by themes
echo "<div class=\"four-columns top-knjige-column\">" . $args['before_widget'];
if ( ! empty( $title ) )
echo $args['before_title'] . $title . $args['after_title'];

// This is where you run the code and display the output
$top_books = explode(',',get_theme_mod( 'custom_top_books' ));

$arg = array(
    'post_type' => 'product',
    'post__in' => $top_books,
    'posts_per_page' => '10',
    'orderby' => 'post__in',
);
$query = new WP_Query( $arg );
if ( $query->have_posts() ) {
    woocommerce_product_loop_start();
        while ( $query->have_posts() ) {
            $query->the_post();
            do_action( 'woocommerce_shop_loop' ); ?>
                <div class="top-knjige-single"><?php wc_get_template_part( 'content', 'top-product' );  ?></div>
            <?php
        }
        wp_reset_query();
	woocommerce_product_loop_end();
	do_action( 'woocommerce_after_shop_loop' );
} else {
	do_action( 'woocommerce_no_products_found' );
}
echo "</div>" . $args['after_widget'];
}

// Widget Backend
public function form( $instance ) {
if ( isset( $instance[ 'title' ] ) ) {
$title = $instance[ 'title' ];
}
else {
$title = __( 'Top knjige', 'wpb_widget_domain' );
}
// Widget admin form
?>
<p>
<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
</p>
<?php
}

// Updating widget replacing old instances with new
public function update( $new_instance, $old_instance ) {
$instance = array();
$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
return $instance;
}
}

/* Currency symbol */
function change_existing_currency_symbol( $currency_symbol, $currency ) {
     switch( $currency ) {
          case 'RSD': $currency_symbol = 'RSD'; break;
     }
     return $currency_symbol;
}
add_filter('woocommerce_currency_symbol', 'change_existing_currency_symbol', 10, 2);

add_filter( 'woocommerce_product_single_add_to_cart_text', 'themeprefix_cart_button_text' );
add_filter( 'woocommerce_product_add_to_cart_text', 'themeprefix_cart_button_text' );

function themeprefix_cart_button_text() {
    return __( 'Dodaj u korpu', 'woocommerce' );
}

function custom_registration_redirect() {
    return home_url('/moj-nalog/hvala');
}
add_action('woocommerce_registration_redirect', 'custom_registration_redirect');

add_action('wp_logout','logout_redirect');

function logout_redirect(){
    wp_redirect( home_url() );
    exit;
}

function bbloomer_redirect_checkout_add_cart( $url ) {
    global $product;
    $product = wc_get_product( 'prod_id' );
    $url = $product;
    return $url;
}

add_filter( 'woocommerce_add_to_cart_redirect', 'bbloomer_redirect_checkout_add_cart' );

class WC_Widget_Recently_Viewed2 extends WC_Widget {

	/**
	 * Constructor.
	 */
	public function __construct() {
		$this->widget_cssclass    = 'woocommerce widget_recently_viewed_products';
		$this->widget_description = __( "Display a list of a customer's recently viewed products.", 'woocommerce' );
		$this->widget_id          = 'woocommerce_recently_viewed_products';
		$this->widget_name        = __( 'Recently viewed', 'woocommerce' );
		$this->settings           = array(
			'title'  => array(
				'type'  => 'text',
				'std'   => __( 'Recently Viewed Products', 'woocommerce' ),
				'label' => __( 'Title', 'woocommerce' ),
			),
			'number' => array(
				'type'  => 'number',
				'step'  => 1,
				'min'   => 1,
				'max'   => 15,
				'std'   => 10,
				'label' => __( 'Number of products to show', 'woocommerce' ),
			),
		);

		parent::__construct();
	}

	/**
	 * Output widget.
	 *
	 * @see WP_Widget
	 * @param array $args     Arguments.
	 * @param array $instance Widget instance.
	 */
	public function widget( $args, $instance ) {
		$viewed_products = ! empty( $_COOKIE['woocommerce_recently_viewed'] ) ? (array) explode( '|', wp_unslash( $_COOKIE['woocommerce_recently_viewed'] ) ) : array(); // @codingStandardsIgnoreLine
		$viewed_products = array_reverse( array_filter( array_map( 'absint', $viewed_products ) ) );

		if ( empty( $viewed_products ) ) {
			return;
		}

		ob_start();

		$number = ! empty( $instance['number'] ) ? absint( $instance['number'] ) : $this->settings['number']['std'];

		$query_args = array(
			'posts_per_page' => $number,
			'no_found_rows'  => 1,
			'post_status'    => 'publish',
			'post_type'      => 'product',
			'post__in'       => $viewed_products,
			'orderby'        => 'post__in',
		);

		if ( 'yes' === get_option( 'woocommerce_hide_out_of_stock_items' ) ) {
			$query_args['tax_query'] = array(
				array(
					'taxonomy' => 'product_visibility',
					'field'    => 'name',
					'terms'    => 'outofstock',
					'operator' => 'NOT IN',
				),
			); // WPCS: slow query ok.
		}

		$r = new WP_Query( apply_filters( 'woocommerce_recently_viewed_products_widget_query_args', $query_args ) );

		if ( $r->have_posts() ) {

			$this->widget_start( $args, $instance );

			echo wp_kses_post( apply_filters( 'woocommerce_before_widget_product_list', '<div class="owl-vec-ste-gledali">' ) );

			$template_args = array(
				'widget_id' => $args['widget_id'],
			);

			while ( $r->have_posts() ) {
				$r->the_post();
				wc_get_template( 'content-widget-product.php', $template_args );
			}

			echo wp_kses_post( apply_filters( 'woocommerce_after_widget_product_list', '</div>' ) );

			$this->widget_end( $args );
		}

		wp_reset_postdata();

		$content = ob_get_clean();

		echo $content; // WPCS: XSS ok.
	}
}

function wpb_load_widget2() {
    register_widget( 'WC_Widget_Recently_Viewed2' );
}
add_action( 'widgets_init', 'wpb_load_widget2' );

function new_loop_shop_per_page( $cols ) {
  $cols = 20;
  return $cols;
}

add_filter( 'loop_shop_per_page', 'new_loop_shop_per_page', 20 );

function patricks_woocommerce_catalog_orderby( $orderby ) {
	unset($orderby["rating"]);
	unset($orderby["popularity"]);
    unset($orderby["menu_order"]);
	return $orderby;
}
add_filter( "woocommerce_catalog_orderby", "patricks_woocommerce_catalog_orderby", 20 );

function patricks_woocommerce_catalog_orderby1( $sortby ) {
	$sortby['title-desc'] = __( 'Abecednom redu, od A do Š', 'woocommerce' );
	return $sortby;
}
add_filter( 'woocommerce_catalog_orderby', 'patricks_woocommerce_catalog_orderby1', 20 );

function patricks_woocommerce_get_catalog_ordering_args( $args ) {
	$orderby_value = isset( $_GET['orderby'] ) ? wc_clean( $_GET['orderby'] ) : apply_filters( 'woocommerce_default_catalog_orderby', get_option( 'woocommerce_default_catalog_orderby' ) );
	if ( 'title-desc' == $orderby_value ) {
		$args['orderby'] = 'title';
		$args['order']   = 'ASC';
	}
	return $args;
}
add_filter( 'woocommerce_get_catalog_ordering_args', 'patricks_woocommerce_get_catalog_ordering_args', 20 );

function patricks_woocommerce_catalog_orderby2( $orderby ) {
	$orderby["relevance"] = __('Relevantnost', 'woocommerce');
	$orderby["price-desc"] = __('Ceni, od veće ka manjoj', 'woocommerce');
    $orderby["price"] = __('Ceni, od manje ka većoj', 'woocommerce');
    $orderby["date"] = __('Datumu dodavanja', 'woocommerce');
	return $orderby;
}
add_filter( "woocommerce_catalog_orderby", "patricks_woocommerce_catalog_orderby2", 20 );

function patricks_woocommerce_catalog_orderby3( $sortby1 ) {
	$sortby1['title'] = __( 'Abecednom redu, od Š do A', 'woocommerce' );
	return $sortby1;
}
add_filter( 'woocommerce_catalog_orderby', 'patricks_woocommerce_catalog_orderby3', 20 );

function patricks_woocommerce_get_catalog_ordering_args3( $args1 ) {
	$orderby_value1 = isset( $_GET['orderby'] ) ? wc_clean( $_GET['orderby'] ) : apply_filters( 'woocommerce_default_catalog_orderby', get_option( 'woocommerce_default_catalog_orderby' ) );
	if ( 'title' == $orderby_value1 ) {
		$args1['orderby'] = 'title';
		$args1['order']   = 'DESC';
	}
	return $args1;
}
add_filter( 'woocommerce_get_catalog_ordering_args', 'patricks_woocommerce_get_catalog_ordering_args3', 20 );

// function my_custom_checkout_field_process() {
//     $secret = '6LcHtGYUAAAAAOlddOrNjqPZl5dRLo-7jQ9hqDP6';
//     $gRecaptchaResponse = $_POST['g-recaptcha-response'];
//     $remoteIp = $_SERVER['REMOTE_ADDR'];
//     $recaptcha = new \ReCaptcha\ReCaptcha( $secret );
//     $resp = $recaptcha->verify( $gRecaptchaResponse, $remoteIp );
//     if ( ! $resp->isSuccess() ) {
//         wc_add_notice( 'Polje reCAPTCHA je obavezno', 'error' );
//     }
// }
// add_action('woocommerce_checkout_process', 'my_custom_checkout_field_process');

add_filter( 'woocommerce_add_to_cart_fragments', 'woocommerce_header_add_to_cart_fragment' );

function woocommerce_header_add_to_cart_fragment( $fragments ) {
	global $woocommerce;

	ob_start();

	?>
        <div id="cart-fragment"><i class="icon-cart"></i> <div class="cart-counter"><span><?php echo WC()->cart->get_cart_contents_count() ?></span></div></div>
	<?php
	$fragments['div#cart-fragment'] = ob_get_clean();
	return $fragments;
}

add_filter( 'woocommerce_add_to_cart_fragments', 'woocommerce_header_add_to_cart_fragment_sec' );

function woocommerce_header_add_to_cart_fragment_sec( $fragments ) {
	global $woocommerce;

	ob_start();

	?>
      <div id="cart-fragment-sec">
        <?php
        foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) { ?>
            <div class="mini-narudzbina">
                <?php
                $_product   = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
                $product_id = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );
                ?>
                <div class="mini-narudzbina-image">
                    <?php
                    $image = wp_get_attachment_image_src( get_post_thumbnail_id( $_product->get_id() ), 'full' );
                    ?>
                    <a href="<?php the_permalink( $product_id ); ?>"><img src="<?php echo $image[0]; ?>" alt="<?php echo $_product->get_name(); ?>" title="<?php echo $_product->get_name(); ?>"></a>
                </div>
                <?php
                $terms = wp_get_post_terms( $_product->get_id(), 'autor' ); ?>
                <div class="mini-narudzbina-author">
                    <?php
                    /* if( $terms ) {
                         ?>
                         <!-- <p class="mini-cart-name"><?php echo $terms[0]->name; ?></p> -->
                         <?php
                    } */
                    ?>
                    <a href="<?php the_permalink( $product_id ); ?>"><p class="mini-cart-book"><?php echo $_product->get_name(); ?></p></a>
                </div>
                <?php
                $targeted_id = $product_id;
                foreach ( WC()->cart->get_cart() as $cart_item ) {
                    if($cart_item['product_id'] == $targeted_id ) {
                        $qty =  $cart_item['quantity'];
                        break;
                    }
                }
                if( ! empty( $qty ) ) {
                    ?>
                    <div class="mini-narudzbina-price">
                        <p><?php echo $qty; ?> x <?php echo WC()->cart->get_product_price( $_product ); ?></p>
                    </div>
                    <?php
                }
                ?>
            </div>
        <?php }
        global $woocommerce;
        ?>
        <div class="mini-cart-ukupno">
            <p class="mini-cart-ukupno-cifra">Ukupno: <span><?php echo $woocommerce->cart->get_cart_total(); ?></span></p>
            <p class="mini-cart-ukupno-napomena">(Troškovi isporuke nisu uračunati)</p>
        </div>
        <div class="mini-cart-buttons">
            <a class="pregled-korpe" href="<?php echo wc_get_cart_url(); ?>">Pregled korpe</a>
            <a class="placanje" href="<?php echo wc_get_checkout_url(); ?>">Plaćanje</a>
        </div>
    </div>
	<?php
	$fragments['div#cart-fragment-sec'] = ob_get_clean();
	return $fragments;
}
