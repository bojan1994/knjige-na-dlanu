<?php

/* Template Name: Hvala */

get_header();

if ( have_posts() ) :
    while ( have_posts() ) :
        the_post(); ?>
        <div class="hvala-container container">
            <h1><?php the_title(); ?></h1>
            <p>Uspešno ste se registrovali.</p>
            <p class="hvala-second-paragraph">Molimo Vas da podesite svoje podatke za isporuku klikom na <a href="<?php echo wc_get_endpoint_url( 'edit-address' ); ?>">ovaj link</a>.</p>
        </div>

    <?php endwhile;
endif;

get_footer();
