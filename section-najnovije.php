<div class="container najnovije-container">
  <div class="naslovna-carousel-naslov">
      <h2 class="najnovije-main-heading">Najnovije</h2>
  </div>
<?php $most_recent = explode(',',get_theme_mod( 'custom_most_recent' )); ?>



<div class="owl-carousel-najnovije owl-carousel owl-theme owl-loaded vrteska">
  <?php global $post;
  $args1 = array(
      'post_type'       => 'product',
      'post__in'        => $most_recent,
      'posts_per_page'  => '12',
      'no_found_rows'   => true,
      'cache_results'          => false,
      'update_post_meta_cache' => false,
      'update_post_term_cache' => false,
  );
  $najnovije = get_posts( $args1 );
  foreach ( $najnovije as $post ) :
    setup_postdata( $post ); ?>
    <div>
      <div class="owl-carousel-single-image">
        <a rel="bookmark" title="<?php the_title(); ?>">
            <?php the_post_thumbnail('medium', array('alt' => ''.get_the_title().'', 'title' => ''.get_the_title().'', 'itemprop' => 'image', 'class' => 'lazy', 'data-src' => ''.get_the_post_thumbnail_url('', 'medium' ).'', )); ?>
            
        <div class="hidden-article unseen">
          <div class="inner-hidden-article">
            <a class="see-more" href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title(); ?>">Vidi vise</a>
            <a href="/?add-to-cart=<?php the_ID(); ?>" data-quantity="1" class="button product_type_simple add_to_cart_button ajax_add_to_cart" data-product_id="<?php the_ID(); ?>" data-product_sku="" aria-label="Add <?php the_title(); ?> to your cart" rel="nofollow" title="Dodaj u korpu">Dodaj u korpu</a>
          </div>
        </div>
          </a>

        </div>
        <a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title(); ?>"><h3 class="naslovna-author-name"><?php the_title(); ?></h3></a>
        <?php global $woocommerce;
                      $currency = get_woocommerce_currency_symbol();
                      $price = get_post_meta( get_the_ID(), '_regular_price', true);
                      $sale = get_post_meta( get_the_ID(), '_sale_price', true);
                      ?>

                      <?php if($sale) : ?>
                      <span class="price"><del><span class="woocommerce-Price-amount amount"><?php echo $price; ?>&nbsp;<span class="woocommerce-Price-currencySymbol"><?php echo $currency; ?></span></span></del> <ins><span class="woocommerce-Price-amount amount"><?php echo $sale; ?>&nbsp;<span class="woocommerce-Price-currencySymbol"><?php echo $currency; ?></span></span></ins> </span>
                      <?php elseif($price) : ?>
                      <span class="price"><span class="woocommerce-Price-amount amount"><?php echo $price; ?>&nbsp;<span class="woocommerce-Price-currencySymbol"><?php echo $currency; ?></span></span></span>
                      <?php endif; ?>
      </div>
  <?php
  endforeach;
  wp_reset_postdata();
  ?>




</div>
</div>
<?php
if( ! wp_is_mobile() ) {
    ?>
    <div class="container banners-container clearfix">
        <div class="two-columns">
            <a href="https://www.knjigenadlanu.com/proizvod/komplet-duhovnih-knjiga/"><img class="img-responsive" src="<?php bloginfo('template_url'); ?>/assets/images/banner1.jpg"></a>
        </div>
        <div class="two-columns">
            <a href="#"><img class="img-responsive" src="<?php bloginfo('template_url'); ?>/assets/images/banner2.jpg"></a>
        </div>
    </div>
    <?php
} else {
    ?>
    <div class="img-responsive container banners-container clearfix">
        <a href="https://www.knjigenadlanu.com/proizvod/komplet-knjiga-javascript-i-php-7-objektivno/#"><img class="img-responsive" src="<?php bloginfo('template_url'); ?>/assets/images/banner5.jpg"></a>
    </div>
    <?php
}
?>
