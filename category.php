<?php
/*
Template Name: Vesti
*/
get_header(); ?>

<div class="container stranica-vesti-container">
<div class="stranica-vesti-select">
<div class="one-column"><h1>Vesti</h1></div>
<div class="stranica-vesti-columns">
<?php
$paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;
$args = array(
    'post_type' => 'post',
    'posts_per_page' => '6',
    'cache_results'          => false,
		'update_post_meta_cache' => false,
		'update_post_term_cache' => false,
    'paged' => $paged,
);
$news_query = new WP_Query( $args ); ?>

<?php while ( $news_query->have_posts() ) :
        $news_query->the_post(); ?>
            <div class="three-columns stranica-vesti-inner-columns">
                <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail( 'news-image', array( 'class' => 'img-responsive', 'alt' => get_the_title(), 'title' => get_the_title() ) ); ?></a>
                <p class="posts-info"><?php the_time('d.m.Y'); ?>.</p>
                <h3><?php the_title(); ?></h3>
                <?php the_excerpt(); ?>
                <a href="<?php the_permalink(); ?>">Više</a>
            </div>
    <?php endwhile; ?>

    <nav class="navigation" role="navigation">
      <?php global $news_query;
            $big = 999999999; // need an unlikely integer
            echo paginate_links(array(
                    'base' => str_replace($big, '%#%', esc_url(get_pagenum_link($big))),
                    'format' => '?paged=%#%',
                    'current' => max(1, get_query_var('paged')),
                    'total' => $news_query->max_num_pages
            )); ?>
    </nav>

    <?php wp_reset_postdata();
?>
</div>
</div>

<?php get_sidebar(); ?>

</div>

<?php get_footer();
