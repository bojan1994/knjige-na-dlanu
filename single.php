<?php

get_header();

if (have_posts()) :
    while (have_posts()) :
        the_post(); ?>
        <div class="single-vest-container container">
            <div class="single-vest-content">
                <div class="single-vest-date one-column">
                    <?php the_time('d.m.Y'); ?>
                </div>
                <div class="single-vest-naslov one-column">
                    <h1><?php the_title(); ?></h1>
                </div>
                <div class="single-vest-content-inner">
                    <div class="single-vest-image clearfix">
                        <?php the_post_thumbnail( 'full', array( 'class' => 'img-responsive', 'alt' => get_the_title(), 'title' => get_the_title() ) ); ?>
                    </div>
                    <div class="single-vest-content-inner-text">
                        <?php the_content(); ?>
                    </div>
                </div>
            </div>

            <?php    endwhile;
            else :
                _e('Sorry, no content found.', 'Knjige na dlanu');
            endif;

            if ( is_active_sidebar( 'sidebar' ) ) { ?>
                <div class="single-vest-sidebar">
                    <?php dynamic_sidebar( 'sidebar' ); ?>
                </div>

            <?php } ?>

        </div>
<?php get_footer();
