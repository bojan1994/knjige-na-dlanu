<div class="knjige-na-akciji-outer-container">
<div class="container knjige-na-akciji-container">
  <div class="naslovna-carousel-naslov">
      <a href="<?php echo home_url('/knjige-na-akciji/'); ?>"><h2>Knjige na akciji</h2></a>
      <a class="pogledaj-sve" href="<?php echo home_url('/knjige-na-akciji/'); ?>" title="<?php _e('Knjige na akciji', 'srkileee-framework'); ?>">pogledaj sve</a>
  </div>
<div class="owl-carousel-knjige-na-akciji owl-carousel owl-theme owl-loaded vrteska">
  <?php global $post;
  $args4 = array(
    'posts_per_page'    => 12,
    'no_found_rows'     => 1,
    'post_status'       => 'publish',
    'post_type'         => 'product',
    'meta_query'        => WC()->query->get_meta_query(),
    'post__in'          => array_merge( array( 0 ), wc_get_product_ids_on_sale() ),
    'order'             => 'ASC',
    'cache_results'          => false,
    'update_post_meta_cache' => false,
    'update_post_term_cache' => false,
  );
  $akcija = get_posts( $args4 );
  foreach ( $akcija as $post ) :
    setup_postdata( $post ); ?>
    <div>
      <div class="owl-carousel-single-image">
        <a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title(); ?>">
            <?php the_post_thumbnail('medium', array('alt' => ''.get_the_title().'', 'title' => ''.get_the_title().'', 'itemprop' => 'image', 'class' => 'lazy', 'data-src' => ''.get_the_post_thumbnail_url('', 'medium' ).'', )); ?>
          </a>
        <a href="/?add-to-cart=<?php the_ID(); ?>" data-quantity="1" class="button product_type_simple add_to_cart_button ajax_add_to_cart" data-product_id="<?php the_ID(); ?>" data-product_sku="" aria-label="Add <?php the_title(); ?> to your cart" rel="nofollow" title="Dodaj u korpu">Dodaj u korpu</a>

        </div>
        <a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title(); ?>"><h3 class="naslovna-author-name"><?php the_title(); ?></h3></a>
        <?php global $woocommerce;
                      $currency = get_woocommerce_currency_symbol();
                      $price = get_post_meta( get_the_ID(), '_regular_price', true);
                      $sale = get_post_meta( get_the_ID(), '_sale_price', true);
                      ?>

                      <?php if($sale) : ?>
                      <span class="price"><del><span class="woocommerce-Price-amount amount"><?php echo $price; ?>&nbsp;<span class="woocommerce-Price-currencySymbol"><?php echo $currency; ?></span></span></del> <ins><span class="woocommerce-Price-amount amount"><?php echo $sale; ?>&nbsp;<span class="woocommerce-Price-currencySymbol"><?php echo $currency; ?></span></span></ins></span>
                      <?php elseif($price) : ?>
                      <span class="price"><span class="woocommerce-Price-amount amount"><?php echo $price; ?>&nbsp;<span class="woocommerce-Price-currencySymbol"><?php echo $currency; ?></span></span></span>
                      <?php endif; ?>


      </div>
  <?php
  endforeach;
  wp_reset_postdata();
  ?>

</div>
</div>
</div>
