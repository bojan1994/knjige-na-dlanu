<?php
if ( is_front_page() ) {
    ?>
    <div class="owl-carousel home-slider owl-loaded">
    <?php global $post;
    $args0 = array(
        'post_type' => 'slider',
        'posts_per_page' => '10',
        'no_found_rows' => true,
        'cache_results'          => false,
        'update_post_meta_cache' => false,
        'update_post_term_cache' => false,
    );
    $slides = get_posts( $args0 );
    foreach ( $slides as $post ) :
      setup_postdata( $post ); ?>
      <div class="slide">
          <a href="<?php echo get_post_meta( $post->ID,'_url_value_key',true ); ?>" title="<?php the_title(); ?>">
            <?php the_post_thumbnail('full', array('alt' => ''.get_the_title().'', 'title' => ''.get_the_title().'', 'itemprop' => 'image', 'class' => 'lazy', 'data-src' => ''.get_the_post_thumbnail_url('', 'full' ).'', )); ?></a>
      </div>
    <?php
    endforeach;
    wp_reset_postdata();
    ?>
  </div>

    <?php
}
