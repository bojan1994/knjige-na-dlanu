<?php

/* Template Name: Kontakt */

get_header();
?>

<div class="contact-container container">
    <?php while( have_posts() ) :
            the_post(); ?>
            <h1><?php the_title(); ?></h1>
            <?php the_content(); ?>
        <?php endwhile; ?>
</div>


<div class="vec-ste-gledali">
    <div class="vec-ste-gledali-container container">
        <?php
        if ( is_active_sidebar( 'sidebar2' ) ) {
            dynamic_sidebar( 'sidebar2' );
        }
        ?>
    </div>
</div>

<?php get_footer();
