module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
	less: {
  		development: {
	    	    options: {
	      		paths: ['default']
    		    },
    		    files: {
      			'style.css': 'assets/less/main.less'
    		    },
	        }
              },
        cssmin: {
          options: {
            mergeIntoShorthands: false,
            roundingPrecision: -1
          },
          target: {
            files: {
              'style.min.css': ['style.css']
            }
          }
        },
        uglify: {
          target: {
            files: {
              'main.min.js': ['assets/scripts/main.js']
            }
          }
        },
        watch: {
  	     css: {
    		files: '**/assets/less/*.less',
    		tasks: ['less', 'cssmin'],
    		options: {
      			livereload: true,
    		},
             },
   	     js: {
     		files: '**/assets/scripts/*.js',
     		tasks: ['uglify'],
     		options: {
       			livereload: true,
     		},
              },
        },
  });

  // Load the plugin that provides the "less" task.
	grunt.loadNpmTasks('grunt-contrib-less');

  // Load the plugin that provides the "cssmin" task.
    grunt.loadNpmTasks('grunt-contrib-cssmin');

  // Load the plugin that provides the "uglify" task.
    grunt.loadNpmTasks('grunt-contrib-uglify');

	// Load the plugin that provides the "watch" task.
	grunt.loadNpmTasks('grunt-contrib-watch');

  // Default task(s).
    grunt.registerTask('default', ['watch']);

};
