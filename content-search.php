<?php

get_header( 'shop' );

global $post;

?>

<div class="container search-results-container clearfix">

    <div class="woocommerce-products-header one-column clearfix">
        <h1><span class="rezultat">Rezultat pretrage</span> "<?php echo get_search_query(); ?>"</h1>

        <?php
        if( $post ) {
            if( ! wp_is_mobile() ) {
                ?>
                <div class="sort-form clearfix">
                    <p><?php do_action( 'woocommerce_before_shop_loop' ); ?><span class="sort">Sortiraj po: </span></p>
                </div>
                <?php
            } else {
                ?>
                <div class="sort-form clearfix">
                    <p><span>Sortiraj po: </span><?php do_action( 'woocommerce_before_shop_loop' ); ?></p>
                </div>
                <?php
            }
        }
        ?>
    </div>
    <div class="container kategorije-container">
        <div class="kategorije-select">
        <?php
        if( have_posts()) :
            while( have_posts() ) :
                the_post();
                $_product = wc_get_product( $post->ID ); ?>

                   <div class="five-columns kategorije-select-column">
                   <div class="owl-carousel-single-image">
                     <a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title(); ?>">
                         <?php the_post_thumbnail('carousel-image', array('alt' => ''.get_the_title().'', 'title' => ''.get_the_title().'', 'itemprop' => 'image', 'class' => 'lazy', 'data-src' => ''.get_the_post_thumbnail_url('', 'carousel-image').'', )); ?>
                     </a>
                     <a href="/?add-to-cart=<?php the_ID(); ?>" data-quantity="1" class="button product_type_simple add_to_cart_button ajax_add_to_cart" data-product_id="<?php the_ID(); ?>" data-product_sku="" aria-label="Add “Pingulingo” to your cart" rel="nofollow" title="Dodaj u korpu">Dodaj u korpu</a>
                     </div>
                     <a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title(); ?>">
                         <h3 class="naslovna-author-name"><?php the_title(); ?></h3>
                     </a>
                     <span class="price"><?php echo $_product->get_price(); ?> RSD</span>
                   </div>
            <?php endwhile; ?>
            <nav class="navigation" role="navigation">
                <?php
                $big = 999999999;
                global $wp_query;
                echo paginate_links(array(
                        'base' => str_replace($big, '%#%', esc_url(get_pagenum_link($big))),
                        'format' => '?paged=%#%',
                        'current' => max(1, get_query_var('paged')),
                        'total' => $wp_query->max_num_pages
                ));
                ?>
            </nav>
            <?php wp_reset_postdata();
        else :
            ?>
            <p class="empty-results">0 pronađenih knjiga</p>
            <?php
        endif;
        ?>
        </div>
    </div>
</div>

<div class="knjige-na-akciji-outer-container">
<div class="container knjige-na-akciji-container">
<div class="naslovna-carousel-naslov">
    <h2>Knjige na akciji</h2>
    <a class="pogledaj-sve" href="<?php echo site_url(); ?>/knjige-na-akciji/">pogledaj sve</a>
</div>
<?php

$args = array(
    'post_type'      => 'product',
    'order'          => 'ASC',
    'posts_per_page' => '12',
    'meta_query'     => array(
        array(
            'key'           => '_sale_price',
            'value'         => 0,
            'compare'       => '>',
            'type'          => 'numeric'
        )
    )
);
$query = new WP_Query( $args );
if ( $query->have_posts() ) {

	/**
	 * Hook: woocommerce_before_shop_loop.
	 *
	 * @hooked wc_print_notices - 10
	 * @hooked woocommerce_result_count - 20
	 * @hooked woocommerce_catalog_ordering - 30
	 */
	//do_action( 'woocommerce_before_shop_loop' );

    woocommerce_product_loop_start(); ?>
        <div class="owl-carousel-knjige-na-akciji owl-carousel owl-theme owl-loaded">
        <?php
        while ( $query->have_posts() ) {
            $query->the_post();
            /**
             * Hook: woocommerce_shop_loop.
             *
             * @hooked WC_Structured_Data::generate_product_data() - 10
             */
            do_action( 'woocommerce_shop_loop' );

            ?>
                <div><?php wc_get_template_part( 'content', 'product' );  ?></div>
            <?php
        }
        wp_reset_query(); ?>
        </div>

	<?php woocommerce_product_loop_end();


	/**
	 * Hook: woocommerce_after_shop_loop.
	 *
	 * @hooked woocommerce_pagination - 10
	 */
	do_action( 'woocommerce_after_shop_loop' );
} else {
	/**
	 * Hook: woocommerce_no_products_found.
	 *
	 * @hooked wc_no_products_found - 10
	 */
	do_action( 'woocommerce_no_products_found' );
}
?></div></div>

<?php

get_footer( 'shop' );
