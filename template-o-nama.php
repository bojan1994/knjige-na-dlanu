<?php

/* Template Name: O nama */

get_header();


    while ( have_posts() ) :
        the_post(); ?>
        <div class="o-nama-container container">
            <h1><?php the_title(); ?></h1>
            <?php the_content(); ?>
        </div>
    <?php endwhile;


get_footer();
