<?php

/* Template name: knjige na akciji */

/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

get_header( 'shop' );

/**
 * Hook: woocommerce_before_main_content.
 *
 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
 * @hooked woocommerce_breadcrumb - 20
 * @hooked WC_Structured_Data::generate_website_data() - 30
 */


?>
<div class="container kategorije-container">
    <div class="six-columns kategorije-menu">
        <?php
        $cat = get_queried_object();
        $catID = $cat->term_id;
        $cat_args = array(
            'orderby'    => 'name',
            'order'      => 'asc',
            'hide_empty' => true,
            'exclude' => array(15,49,52,53),
        );
        $product_categories = get_terms( 'product_cat', $cat_args );
        echo '<h2 class="kategorije-heading">Kategorije</h2>';
        echo '<ul>';
        if( ! empty( $product_categories ) ){
            foreach ( $product_categories as $key => $category ) {
                echo '<li><a href="' . get_term_link( $category ) . '"><span><h2>' . $category->name . ' </h2> (' . $category->count . ')</span></a></li>';
            }
        }
        echo '</ul>';
        ?>
    </div>

    <div class="kategorije-select">
        <div class="kategorije-naslov"><header class="woocommerce-products-header one-column">
	<?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>
		<h1 class="woocommerce-products-header__title page-title">Knjige na akciji</h1>
        <p>Za naše posetioce i verne kupce internet knjižara Knjige na dlanu izdvaja omiljene knjige na akciji! Uživajte čitajući knjige i budite uvek uvek u toku sa aktuelnim akcijama!</p>
        <?php do_action( 'woocommerce_before_main_content' ); ?>
	<?php endif; ?>

	<?php
	/**
	 * Hook: woocommerce_archive_description.
	 *
	 * @hooked woocommerce_taxonomy_archive_description - 10
	 * @hooked woocommerce_product_archive_description - 10
	 */
	do_action( 'woocommerce_archive_description' );
	?>
</header>
</div>
<?php

$args = array(
    'post_type'      => 'product',
    'order'          => 'ASC',
    'posts_per_page' => '12',
		'no_found_rows'   => true,
		'cache_results'          => false,
		'update_post_meta_cache' => false,
		'update_post_term_cache' => false,
    'meta_query'     => array(
        array(
            'key'           => '_sale_price',
            'value'         => 0,
            'compare'       => '>',
            'type'          => 'numeric'
        )
    )
);
$query = new WP_Query( $args );

if ( $query->have_posts() ) {

	/**
	 * Hook: woocommerce_before_shop_loop.
	 *
	 * @hooked wc_print_notices - 10
	 * @hooked woocommerce_result_count - 20
	 * @hooked woocommerce_catalog_ordering - 30
	 */
	//do_action( 'woocommerce_before_shop_loop' );

	woocommerce_product_loop_start(); ?>
        <?php while ( $query->have_posts() ) {
                $query->the_post();
                /**
                 * Hook: woocommerce_shop_loop.
                 *
                 * @hooked WC_Structured_Data::generate_product_data() - 10
                 */
                do_action( 'woocommerce_shop_loop' );

                ?>
                    <div class="five-columns kategorije-select-column">
                        <div><?php wc_get_template_part( 'content', 'product' );  ?></div>
                    </div>
                <?php
            }

        wp_reset_postdata(); ?>

	<?php woocommerce_product_loop_end();

	/**
	 * Hook: woocommerce_after_shop_loop.
	 *
	 * @hooked woocommerce_pagination - 10
	 */
	do_action( 'woocommerce_after_shop_loop' );
} else {
	/**
	 * Hook: woocommerce_no_products_found.
	 *
	 * @hooked wc_no_products_found - 10
	 */
	do_action( 'woocommerce_no_products_found' );
}

?></div></div>

<?php

/**
 * Hook: woocommerce_after_main_content.
 *
 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
 */
//do_action( 'woocommerce_after_main_content' );

/**
 * Hook: woocommerce_sidebar.
 *
 * @hooked woocommerce_get_sidebar - 10
 */
do_action( 'woocommerce_sidebar' );

get_footer( 'shop' );
