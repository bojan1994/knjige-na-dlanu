<?php get_header(); ?>
<!--main-->
<main id="content" class="main main-home">
  <div id="primary" class="main-home-primary">
  <?php get_template_part('section', 'najnovije'); ?>
  <?php get_template_part('section', 'popularno'); ?>
  <?php get_template_part('section', 'preporucujemo'); ?>
  <?php get_template_part('section', 'akcija'); ?>
  <?php get_template_part('section', 'vesti'); ?>
  </div>
</main>
<!--end of main-->
<?php get_footer(); ?>
